force -freeze sim:/son_clock_2hz/clk 1 0, 0 {7812500 ps} -r {15625 ns}
# Test start_2hz à 1
force -freeze sim:/son_clock_2hz/rst 0 0
force -freeze sim:/son_clock_2hz/start_2hz 1 0
run 1000 ms
# Test start_2hz et reset à 1
force -freeze sim:/son_clock_2hz/rst 1 0
force -freeze sim:/son_clock_2hz/start_2hz 1 0
run 1000 ms
#Test reset à 1
force -freeze sim:/son_clock_2hz/rst 1 0
force -freeze sim:/son_clock_2hz/start_2hz 0 0
run 1000 ms