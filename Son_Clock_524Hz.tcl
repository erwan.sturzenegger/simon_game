force -freeze sim:/son_clock_524hz/clk 1 0, 0 {7812500 ps} -r {15625 ns}
# Test état 524hz
force -freeze sim:/son_clock_524hz/rst 0 0
force -freeze sim:/son_clock_524hz/etat_son 0111 0
run 1000 ms
# Test reset
force -freeze sim:/son_clock_524hz/rst 1 0
force -freeze sim:/son_clock_524hz/etat_son 0000 0
run 1000 ms
# Test état 524hz
force -freeze sim:/son_clock_524hz/rst 0 0
force -freeze sim:/son_clock_524hz/etat_son 0111 0
run 1000 ms
# Test état autre
force -freeze sim:/son_clock_524hz/rst 0 0
force -freeze sim:/son_clock_524hz/etat_son 1010 0
run 1000 ms