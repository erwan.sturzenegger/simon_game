----------------------------------------------------------------------------------
-- Module Name:    LEDIndicatives - Behavioral
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity LEDIndicatives is
    Port ( etat_systeme : in STD_LOGIC;
           etat_jeu : in  STD_LOGIC_VECTOR (2 downto 0);
           led_joueur : out  STD_LOGIC;
           led_sequence : out  STD_LOGIC);
end LEDIndicatives;

architecture Behavioral of LEDIndicatives is

begin

 led_state: process(etat_jeu, etat_systeme)
 begin
   case etat_systeme is
    when '0' =>
      led_joueur <= '1';
      led_sequence <= '0';
    when others =>
      case (etat_jeu) is
       when "000" | "001" | "011" | "100" | "101" =>
        led_joueur <= '0';
        led_sequence <= '1';
      when others =>
        led_joueur <= '1';
        led_sequence <= '0';
      end case;
    end case;
end process;

end Behavioral;