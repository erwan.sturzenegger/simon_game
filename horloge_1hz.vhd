----------------------------------------------------------------------------------
-- Company:
-- Engineer: Erwan Sturzenegger
--
-- Create Date:    09:31:05 05/12/2021
-- Design Name:  Projet_integre
-- Module Name:    horloge_1hz - Behavioral
-- Project Name: Projet intégré 2021 - Groupe 5
-- Target Devices:
-- Tool versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------
LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;
ENTITY horloge_1hz IS
  PORT
  (
    clk            : IN  STD_LOGIC;
    rst            : IN  STD_LOGIC;
    horloge_rst    : IN  STD_LOGIC;
    horloge_rst_ok : OUT STD_LOGIC;
    etat_jeu       : IN  STD_LOGIC_VECTOR (2 DOWNTO 0);
    signal_1Hz     : OUT STD_LOGIC);
END horloge_1hz;

ARCHITECTURE Behavioral OF horloge_1hz IS
  CONSTANT demi_seconde                   : unsigned(14 DOWNTO 0) := to_unsigned(32000, 15);
  SIGNAL compteur_present, compteur_futur : unsigned(14 DOWNTO 0);
  SIGNAL fin_comptage                     : STD_LOGIC;
  TYPE etats IS (haut, bas);
  SIGNAL etat_present, etat_futur         : etats;
  SIGNAL reset                            : STD_LOGIC;
  SIGNAL etat_jeu_present, etat_jeu_futur : STD_LOGIC_VECTOR(2 DOWNTO 0);
BEGIN
  fin_comptage <= '1' WHEN compteur_present = demi_seconde ELSE
    '0';

  registre_machine_etat : PROCESS (clk, rst)
  BEGIN
    IF (rst = '1') THEN
      etat_present <= bas;
    ELSIF (rising_edge(clk)) THEN
      etat_present <= etat_futur;
    END IF;
  END PROCESS;

  combi_machine_etat : PROCESS (etat_present, fin_comptage, reset, horloge_rst)
  BEGIN
    CASE(etat_present) IS
      WHEN haut =>
      IF (fin_comptage = '1' OR reset = '1') THEN
        etat_futur <= bas;
        IF horloge_rst = '1' THEN
          horloge_rst_ok <= '1';
        ELSE
          horloge_rst_ok <= '0';
        END IF;
      ELSE
        etat_futur     <= haut;
        horloge_rst_ok <= '0';
      END IF;

      WHEN bas =>
      IF (fin_comptage = '1' AND reset = '0') THEN
        etat_futur     <= haut;
        horloge_rst_ok <= '0';
      ELSE
        etat_futur <= bas;
        IF horloge_rst = '1' THEN
          horloge_rst_ok <= '1';
        ELSE
          horloge_rst_ok <= '0';
        END IF;
      END IF;

      WHEN OTHERS =>
      etat_futur <= bas;
      IF horloge_rst = '1' THEN
        horloge_rst_ok <= '1';
      ELSE
        horloge_rst_ok <= '0';
      END IF;
    END CASE;
  END PROCESS;

  registre_compteur : PROCESS (clk, rst)
  BEGIN
    IF (rst = '1') THEN
      compteur_present <= (OTHERS => '0');
    ELSIF (rising_edge(clk)) THEN
      compteur_present <= compteur_futur;
    END IF;
  END PROCESS;

  combi_compteur : PROCESS (compteur_present, fin_comptage, reset)
  BEGIN
    IF fin_comptage = '1' OR reset = '1' THEN
      compteur_futur <= (OTHERS => '0');
    ELSE
      compteur_futur <= compteur_present + 1;
    END IF;
  END PROCESS;

  registre_jeu : PROCESS (clk, rst)
  BEGIN
    IF (rst = '1') THEN
      etat_jeu_present <= "000";
    ELSIF (rising_edge(clk)) THEN
      etat_jeu_present <= etat_jeu_futur;
    END IF;
  END PROCESS;

  combi_jeu : PROCESS (etat_jeu, etat_jeu_present, horloge_rst)
  BEGIN
    IF (etat_jeu_present = etat_jeu AND horloge_rst = '0') THEN
      reset <= '0';
    ELSE
      reset <= '1';
    END IF;
    etat_jeu_futur <= etat_jeu;
  END PROCESS;

  signal_1Hz <= '1' WHEN etat_present = haut ELSE
    '0';

END Behavioral;