----------------------------------------------------------------------------------
-- Company: HEIA-FR
-- Engineer: Naël Telfser
-- 
-- Create Date:    11:53:36 05/19/2021 
-- Design Name: 	Simon_Game
-- Module Name:    Son_Clock_2Hz - Behavioral 
-- Project Name: 	TN2 - Projet Inegré - Groupe 5
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Son_Clock_2Hz is
    Port ( clk : in  STD_LOGIC;
           rst : in  STD_LOGIC;
           start_2hz : in  STD_LOGIC;
           signal_2hz : out  STD_LOGIC);
end Son_Clock_2Hz;

architecture Behavioral of Son_Clock_2Hz is
	type etats is (bas, haut);

	signal etat_present_2hz, etat_futur_2hz : etats;
	signal compteur_present_2hz, compteur_futur_2hz : Unsigned(13 downto 0) := (others => '0');
	constant quart_seconde : Unsigned(13 downto 0) := to_unsigned(16000, 14);
	signal compte_2hz, fin_comptage_2hz : std_logic;
begin
	compte_2hz <= '1' when start_2hz = '1' else '0';
	fin_comptage_2hz <= '1' when compteur_present_2hz = quart_seconde else '0';

	combi_2hz : process(etat_present_2hz, compte_2hz, fin_comptage_2hz)
	begin
		case( etat_present_2hz ) is
		
			when bas =>
				if fin_comptage_2hz = '1' then
					etat_futur_2hz <= haut;
				else
					etat_futur_2hz <= bas;
				end if ;
			when haut =>
				if compte_2hz = '0' or fin_comptage_2hz = '1' then
					etat_futur_2hz <= bas;
				else
					etat_futur_2hz <= haut;
				end if ;
			when others => etat_futur_2hz <= bas;
		end case ;
	end process;

	registre_2hz: process(clk, rst)
	begin
		if rst = '1' then
			etat_present_2hz <= bas;
		elsif rising_edge(clk) then
			etat_present_2hz <= etat_futur_2hz;
		end if ;
	end process;

	registre_compteur_2hz: process(clk, rst)
	begin
		if rst = '1' then
			compteur_present_2hz <= (others => '0');
		elsif rising_edge(clk) then
			compteur_present_2hz <= compteur_futur_2hz;
		end if ;
	end process;

	combi_compteur_2hz: process(compteur_present_2hz, compte_2hz, fin_comptage_2hz)
	begin
		if compte_2hz = '0' or fin_comptage_2hz = '1' then
			compteur_futur_2hz <= (others => '0');
		else
			compteur_futur_2hz <= compteur_present_2hz + 1;
		end if ;
	end process;
	signal_2hz <= '1' when etat_present_2hz = haut else '0';
end Behavioral;

