----------------------------------------------------------------------------------
-- Company:			HEIA-FR
-- Engineer: 		Naël Telfser
-- 
-- Create Date:    	14:28:32 05/18/2021 
-- Design Name:    	TN2 Projet integré - Groupe 5
-- Module Name:    	Compteur_Sequence - Behavioral 
-- Project Name: 
-- Target Devices:  
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Compteur_Sequence is
    Port ( clk : in  STD_LOGIC;
           rst : in  STD_LOGIC;
           etat_jeu : in  STD_LOGIC_VECTOR (2 downto 0);
           no_sequence : out  STD_LOGIC_VECTOR (4 downto 0));
end Compteur_Sequence;

architecture Behavioral of Compteur_Sequence is
	signal no_sequence_present, no_sequece_futur : Unsigned(4 downto 0);
	signal etat_jeu_present, etat_jeu_futur: STD_LOGIC_VECTOR(2 downto 0);
begin
	
	registre: process(clk, rst)
	begin
		if rst = '1' then
			no_sequence_present <= "00000";
		elsif rising_edge(clk) then
			no_sequence_present <= no_sequece_futur;
		end if;
	end process registre;

	process(etat_jeu_futur, etat_jeu_present, no_sequence_present)
	begin
		if etat_jeu_futur = "001" and etat_jeu_futur /= etat_jeu_present then
			no_sequece_futur <= no_sequence_present + 1;
		elsif etat_jeu_futur = "000" then
			no_sequece_futur <= "00000";
		else
			no_sequece_futur <= no_sequence_present;
		end if ;
	end process;

	registre_jeu:process(clk, rst)
	begin
		if(rst='1') then
			etat_jeu_present <= "000";
		elsif(rising_edge(clk)) then
			etat_jeu_present <= etat_jeu_futur;
		end if;
	end process;


	combi_jeu:process(etat_jeu)
	begin
		etat_jeu_futur <= etat_jeu;
	end process;

	no_sequence <= std_logic_vector(no_sequence_present);
end Behavioral;

