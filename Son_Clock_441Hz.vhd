----------------------------------------------------------------------------------
-- Company: HEIA-FR
-- Engineer: Naël Telfser
-- 
-- Create Date:   11:58:38 05/19/2021 
-- Design Name: 	Simon_Game
-- Module Name:    Son_Clock_441Hz - Behavioral 
-- Project Name: 	TN2 - Projet Inegré - Groupe 5
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Son_Clock_441Hz is
    Port ( clk : in  STD_LOGIC;
           rst : in  STD_LOGIC;
           etat_son : in  STD_LOGIC_VECTOR (3 downto 0);
           signal_441hz : out  STD_LOGIC);
end Son_Clock_441Hz;

architecture Behavioral of Son_Clock_441Hz is
	type etats is (bas, haut);

	signal etat_present_441hz, etat_futur_441hz : etats;
	signal compteur_present_441hz, compteur_futur_441hz : Unsigned(6 downto 0) := (others => '0');
	constant half_period_441hz : Unsigned(6 downto 0) := to_unsigned(72, 7);
	signal compte_441hz, fin_comptage_441hz : std_logic := '0';


	
begin
	
	compte_441hz <= '1' when etat_son = "1000" else '0';
	fin_comptage_441hz <= '1' when compteur_present_441hz = half_period_441hz else '0';

	combi_441hz : process(etat_present_441hz, compte_441hz, fin_comptage_441hz)
	begin
		case( etat_present_441hz ) is
		
			when bas =>
				if fin_comptage_441hz = '1' then
					etat_futur_441hz <= haut;
				else
					etat_futur_441hz <= bas;
				end if ;
			when haut =>
				if compte_441hz = '0' or fin_comptage_441hz = '1' then
					etat_futur_441hz <= bas;
				else
					etat_futur_441hz <= haut;
				end if ;
			when others => etat_futur_441hz <= bas;
		end case ;
	end process;

	registre_441hz: process(clk, rst)
	begin
		if rst = '1' then
			etat_present_441hz <= bas;
		elsif rising_edge(clk) then
			etat_present_441hz <= etat_futur_441hz;
		end if ;
	end process;

	registre_compteur_441hz: process(clk, rst)
	begin
		if rst = '1' then
			compteur_present_441hz <= (others => '0');
		elsif rising_edge(clk) then
			compteur_present_441hz <= compteur_futur_441hz;
		end if ;
	end process;

	combi_compteur_441hz: process(compteur_present_441hz, compte_441hz, fin_comptage_441hz)
	begin
		if compte_441hz = '0' or fin_comptage_441hz = '1' then
			compteur_futur_441hz <= (others => '0');
		else
			compteur_futur_441hz <= compteur_present_441hz + 1;
		end if ;
	end process;

	signal_441hz <= '1' when etat_present_441hz = haut else '0';
end Behavioral;

