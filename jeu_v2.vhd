----------------------------------------------------------------------------------
-- Create Date:    16:16:57 05/16/2021
-- Module Name:    Jeu - Behavioral
----------------------------------------------------------------------------------
LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY Jeu IS
  PORT
  (
    clk                     : IN  STD_LOGIC;
    rst                     : IN  STD_LOGIC;
    etat_systeme            : IN  STD_LOGIC;
    gagne                   : IN  STD_LOGIC;
    perdu                   : IN  STD_LOGIC;
    fin_sequence            : IN  STD_LOGIC;
    no_sequence             : IN  STD_LOGIC_VECTOR(4 DOWNTO 0);
    fin_generation_sequence : IN  STD_LOGIC;
    etat_jeu                : OUT STD_LOGIC_VECTOR (2 DOWNTO 0));
END Jeu;

ARCHITECTURE Behavioral OF Jeu IS
  TYPE etat_jeu_type IS (Arret, AfficherSequence, JoueurJoue, Gagne_state, Perdu_state, GenereSequence);
  SIGNAL etat_present, etat_futur : etat_jeu_type;
BEGIN

  WITH etat_present SELECT etat_jeu <=
    "000" WHEN Arret,
    "001" WHEN AfficherSequence,
    "010" WHEN JoueurJoue,
    "011" WHEN Gagne_state,
    "100" WHEN Perdu_state,
    "101" WHEN GenereSequence;

  PROCESS (clk, rst)
  BEGIN
    IF rst = '1' THEN
      etat_present <= Arret;
    ELSIF rising_edge(clk) THEN
      etat_present <= etat_futur;
    END IF;
  END PROCESS;

  PROCESS (etat_present, etat_systeme, gagne, perdu, fin_sequence, fin_generation_sequence, no_sequence)
  BEGIN
    IF (etat_systeme = '0') THEN
      etat_futur <= Arret;
    ELSE
      CASE etat_present IS
        WHEN Arret =>
          IF etat_systeme = '1' THEN
            etat_futur <= GenereSequence;
          ELSE
            etat_futur <= Arret;
          END IF;
        WHEN GenereSequence =>
          IF fin_generation_sequence = '1' THEN
            etat_futur <= AfficherSequence;
          ELSE
            etat_futur <= GenereSequence;
          END IF;
        WHEN AfficherSequence =>
          IF fin_sequence = '1' THEN
            etat_futur <= JoueurJoue;
          ELSE
            etat_futur <= AfficherSequence;
          END IF;
        WHEN JoueurJoue =>
          IF gagne = '1' THEN
            IF (no_sequence = "10000") THEN
              etat_futur <= Gagne_state;
            ELSE
              etat_futur <= AfficherSequence;
            END IF;
          ELSIF perdu = '1' THEN
            etat_futur <= Perdu_state;
          ELSE
            etat_futur <= JoueurJoue;
          END IF;
        WHEN Gagne_state | Perdu_state =>
          IF fin_sequence = '1' THEN
            etat_futur <= Arret;
          ELSE
            etat_futur <= etat_present;
          END IF;
        WHEN OTHERS => etat_futur <= Arret;
      END CASE;
    END IF;
  END PROCESS;
END Behavioral;