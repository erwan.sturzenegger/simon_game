----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    08:00:48 05/19/2021 
-- Design Name:    Projet_integre
-- Module Name:    verificateur_start - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;

ENTITY verificateur_start IS
	PORT
	(
		clk            : IN  STD_LOGIC;
		rst            : IN  STD_LOGIC;
		btn_presse     : IN  STD_LOGIC_VECTOR (5 DOWNTO 0);
		btn_relache    : IN  STD_LOGIC_VECTOR (5 DOWNTO 0);
		etat_jeu       : IN  STD_LOGIC_VECTOR (2 DOWNTO 0);
		horloge_rst_ok : IN  STD_LOGIC;
		signal_1Hz     : IN  STD_LOGIC;
		start          : OUT STD_LOGIC;
		horloge_rst    : OUT STD_LOGIC);
END verificateur_start;

ARCHITECTURE Behavioral OF verificateur_start IS
	TYPE etats IS (e_start, aucune_sortie, start_presse, start_relache);
	CONSTANT start_time                     : unsigned(1 DOWNTO 0) := to_unsigned(2, 2);
	SIGNAL etat_present, etat_futur         : etats                := aucune_sortie;
	SIGNAL compteur_present, compteur_futur : Unsigned(1 DOWNTO 0) := (OTHERS => '0');
	SIGNAL fin_comptage_start               : STD_LOGIC;

BEGIN

	start <= '1' WHEN etat_present = e_start ELSE
		'0';

	fin_comptage_start <= '1' WHEN compteur_present = start_time ELSE
		'0';

	horloge_rst <= '1' when etat_present = start_relache or (etat_present = aucune_sortie AND etat_jeu = "000") else 
		'0';

	registre : PROCESS (clk, rst)
	BEGIN
		IF (rst = '1') THEN
			etat_present <= aucune_sortie;
		ELSIF (rising_edge(clk)) THEN
			etat_present <= etat_futur;
		END IF;
	END PROCESS;

	PROCESS (etat_present, btn_presse, btn_relache, etat_jeu, fin_comptage_start)
	BEGIN
		CASE(etat_present) IS

			WHEN aucune_sortie =>
			--Commande Start
			IF ((btn_presse = "011100") AND etat_jeu = "000") THEN
				etat_futur <= start_presse;
			ELSE
				etat_futur <= aucune_sortie;
			END IF;
			WHEN start_presse =>
			IF (btn_relache = "011100") THEN
				etat_futur <= start_relache;
			ELSIF (fin_comptage_start = '1') THEN
				etat_futur <= e_start;
			ELSE
				etat_futur <= start_presse;
			END IF;
			WHEN start_relache =>
			etat_futur <= aucune_sortie;
			WHEN e_start =>
			IF etat_jeu = "000" THEN
				etat_futur <= e_start;
			ELSE
				etat_futur <= aucune_sortie;
			END IF;
			WHEN OTHERS =>
			etat_futur <= aucune_sortie;
		END CASE;
	END PROCESS;

--	PROCESS (etat_present)
--	BEGIN
--		CASE etat_present IS
--			WHEN start_presse  => horloge_rst  <= '0';
--			WHEN start_relache => horloge_rst <= '1';
--			WHEN OTHERS        => horloge_rst        <= '0';
--		END CASE;
--	END PROCESS;

	PROCESS (clk, rst)
	BEGIN
		IF (rst = '1') THEN
			compteur_present <= (OTHERS => '0');
		ELSIF rising_edge(clk) THEN
			compteur_present <= compteur_futur;
		END IF;
	END PROCESS;

	PROCESS (compteur_present, horloge_rst_ok, signal_1Hz)
	BEGIN

		IF (horloge_rst_ok = '1') THEN
			compteur_futur <= (OTHERS => '0');
		ELSIF horloge_rst_ok = '0' AND signal_1Hz = '1' AND compteur_present = "00" THEN
			compteur_futur <= compteur_present + 1;
		ELSIF horloge_rst_ok = '0' AND signal_1Hz = '0' AND compteur_present = "01" THEN
			compteur_futur <= compteur_present + 1;
		ELSE
			compteur_futur <= compteur_present;
		END IF;

	END PROCESS;

END Behavioral;