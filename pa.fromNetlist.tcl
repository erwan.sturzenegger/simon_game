
# PlanAhead Launch Script for Post-Synthesis pin planning, created by Project Navigator

create_project -name Simon_Game -dir "Z:/Simon_Game/planAhead_run_1" -part xc3s50atq144-4
set_property design_mode GateLvl [get_property srcset [current_run -impl]]
set_property edif_top_file "Z:/Simon_Game/SimonGame.ngc" [ get_property srcset [ current_run ] ]
add_files -norecurse { {Z:/Simon_Game} }
set_param project.pinAheadLayout  yes
set_param project.paUcfFile  "SimonGame.ucf"
add_files "SimonGame.ucf" -fileset [get_property constrset [current_run]]
open_netlist_design
