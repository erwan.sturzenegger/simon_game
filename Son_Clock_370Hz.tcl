force -freeze sim:/son_clock_370hz/clk 1 0, 0 {7812500 ps} -r {15625 ns}
# Test état 370hz
force -freeze sim:/son_clock_370hz/rst 0 0
force -freeze sim:/son_clock_370hz/etat_son 0101 0
run 1000 ms
# Test reset
force -freeze sim:/son_clock_370hz/rst 1 0
force -freeze sim:/son_clock_370hz/etat_son 0000 0
run 1000 ms
# Test état 370hz
force -freeze sim:/son_clock_370hz/rst 0 0
force -freeze sim:/son_clock_370hz/etat_son 0101 0
run 1000 ms
# Test autre état
force -freeze sim:/son_clock_370hz/rst 0 0
force -freeze sim:/son_clock_370hz/etat_son 1010 0
run 1000 ms