----------------------------------------------------------------------------------
-- Company:
-- Engineer:
--
-- Create Date:    11:20:40 06/15/2021
-- Design Name:
-- Module Name:    SimonGame - Behavioral
-- Project Name:
-- Target Devices:
-- Tool versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------
LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

ENTITY SimonGame IS
  PORT (
    clk          : IN  STD_LOGIC;
    rst          : IN  STD_LOGIC;
    btn_gauche   : IN  STD_LOGIC;
    btn_droite   : IN  STD_LOGIC;
    btn_centre   : IN  STD_LOGIC;
    btn_haut     : IN  STD_LOGIC;
    btn_bas      : IN  STD_LOGIC;
    son          : OUT STD_LOGIC;
    led_joueur   : OUT STD_LOGIC;
    led_sequence : OUT STD_LOGIC;
    unite        : OUT STD_LOGIC_VECTOR (6 DOWNTO 0);
    dizaine      : OUT STD_LOGIC_VECTOR (6 DOWNTO 0);
    ligne        : OUT STD_LOGIC_VECTOR (7 DOWNTO 0);
    colonne      : OUT STD_LOGIC_VECTOR (7 DOWNTO 0));
END SimonGame;

ARCHITECTURE Behavioral OF SimonGame IS
  SIGNAL etat_jeu, symbole : STD_LOGIC_VECTOR(2 DOWNTO 0);
  SIGNAL btn_presse, btn_relache : STD_LOGIC_VECTOR(5 DOWNTO 0);
  SIGNAL etat_son : STD_LOGIC_VECTOR(3 DOWNTO 0);
  SIGNAL no_sequence : STD_LOGIC_VECTOR(4 DOWNTO 0);
  SIGNAL sequence : STD_LOGIC_VECTOR(47 DOWNTO 0);
  SIGNAL start, etat_systeme, fin_generation_sequence, gagne, perdu, fin_sequence,
  signal_1Hz, horloge_rst, horloge_rst_ok, signal_2hz,
  signal_3_3hz, signal_209hz, signal_252hz, signal_310hz, signal_370hz,
  signal_415hz, signal_524hz, signal_441hz, signal_262hz, start_2hz
  : STD_LOGIC;
  SIGNAL debug : STD_LOGIC_VECTOR (4 DOWNTO 0);
BEGIN
  mainCtrl : ENTITY work.main(Behavioral)
    PORT MAP(
      clk          => clk,
      rst          => rst,
      etat_jeu     => etat_jeu,
      start        => start,
      etat_systeme => etat_systeme
    );

  jeu : ENTITY work.jeu(Behavioral)
    PORT MAP(
      clk                     => clk,
      rst                     => rst,
      etat_systeme            => etat_systeme,
      fin_generation_sequence => fin_generation_sequence,
      gagne                   => gagne,
      perdu                   => perdu,
      fin_sequence            => fin_sequence,
      no_sequence             => no_sequence,
      etat_jeu                => etat_jeu
    );

  boutons_controller : ENTITY work.boutons_controller(Behavioral)
    PORT MAP(
      clk          => clk,
      rst          => rst,
      btn_haut     => btn_haut,
      btn_bas      => btn_bas,
      btn_gauche   => btn_gauche,
      btn_droite   => btn_droite,
      btn_centre   => btn_centre,
      btn_presse   => btn_presse,
      btn_relache  => btn_relache
    );

  clock_1hz : ENTITY work.horloge_1hz(Behavioral)
    PORT MAP(
      clk            => clk,
      rst            => rst,
      horloge_rst    => horloge_rst,
      signal_1Hz     => signal_1Hz,
      horloge_rst_ok => horloge_rst_ok,
      etat_jeu       => etat_jeu
    );

  GenerateurSymboles : ENTITY work.GenerateurSymboles(Behavioral)
    PORT MAP(
      clk          => clk,
      rst          => rst,
      etat_jeu     => etat_jeu,
      no_sequence  => no_sequence,
      sequence     => sequence,
      symbole      => symbole,
      debug => debug,
      fin_sequence => fin_sequence
    );

  compteur_sequence : ENTITY work.compteur_sequence(Behavioral)
    PORT MAP(
      clk         => clk,
      rst         => rst,
      etat_jeu    => etat_jeu,
      no_sequence => no_sequence
    );

  generateur_sequence : ENTITY work.generateur_sequence(Behavioral)
    PORT MAP(
      clk                     => clk,
      rst                     => rst,
      etat_jeu                => etat_jeu,
      sequence                => sequence,
      fin_generation_sequence => fin_generation_sequence
    );

  verificateur_entree : ENTITY work.verification_entrees(Behavioral)
    PORT MAP(
      clk         => clk,
      rst         => rst,
      sequence    => sequence,
      btn_presse  => btn_presse,
      etat_jeu    => etat_jeu,
      no_sequence => no_sequence,
      gagne       => gagne,
      perdu       => perdu,
      btn_relache => btn_relache
    );

  verificateur_start : ENTITY work.verificateur_start(Behavioral)
    PORT MAP(
      clk            => clk,
      rst            => rst,
      btn_presse     => btn_presse,
      btn_relache    => btn_relache,
      etat_jeu       => etat_jeu,
      horloge_rst_ok => horloge_rst_ok,
      signal_1Hz     => signal_1Hz,
      start          => start,
      horloge_rst    => horloge_rst
    );

  affichage_matrice : ENTITY work.afficheur_matrice(Behavioral)
    PORT MAP(
      clk        => clk,
      rst        => rst,
      symbole    => symbole,
      btn_presse => btn_presse,
      etat_jeu   => etat_jeu,
      ligne      => ligne,
      colonne    => colonne
    );

  affichage_2x7_segments : ENTITY work.affichage_2x7_segments(Behavioral)
    PORT MAP(
      etat_jeu     => etat_jeu,
      etat_systeme => etat_systeme,
      no_sequence  => no_sequence,
      unite        => unite,
      dizaine      => dizaine
    );

  LEDIndicatives : ENTITY work.LEDIndicatives(Behavioral)
    PORT MAP(
      etat_jeu     => etat_jeu,
      led_joueur   => led_joueur,
      led_sequence => led_sequence,
      etat_systeme => etat_systeme
    );

  generateur_sons : ENTITY work.generateur_son(Behavioral)
    PORT MAP(
      clk          => clk,
      rst          => rst,
      etat_jeu     => etat_jeu,
      btn_presse   => btn_presse,
      symbole      => symbole,
      signal_2hz   => signal_2hz,
      signal_3_3hz => signal_3_3hz,
      signal_209hz => signal_209hz,
      signal_252hz => signal_252hz,
      signal_310hz => signal_310hz,
      signal_370hz => signal_370hz,
      signal_415hz => signal_415hz,
      signal_524hz => signal_524hz,
      signal_441hz => signal_441hz,
      signal_262hz => signal_262hz,
      son          => son,
      etat_son     => etat_son,
      start_2hz    => start_2hz
    );

  clock_2hz : ENTITY work.son_clock_2hz(Behavioral)
    PORT MAP(
      clk        => clk,
      rst        => rst,
      start_2hz  => start_2hz,
      signal_2hz => signal_2hz
    );

  clock_3_3hz : ENTITY work.son_clock_3_3hz(Behavioral)
    PORT MAP(
      clk          => clk,
      rst          => rst,
      etat_son     => etat_son,
      signal_3_3hz => signal_3_3hz
    );

  clock_209hz : ENTITY work.son_clock_209hz(Behavioral)
    PORT MAP(
      clk          => clk,
      rst          => rst,
      etat_son     => etat_son,
      signal_209hz => signal_209hz
    );

  clock_252hz : ENTITY work.son_clock_252hz(Behavioral)
    PORT MAP(
      clk          => clk,
      rst          => rst,
      etat_son     => etat_son,
      signal_252hz => signal_252hz
    );

  clock_310hz : ENTITY work.son_clock_310hz(Behavioral)
    PORT MAP(
      clk          => clk,
      rst          => rst,
      etat_son     => etat_son,
      signal_310hz => signal_310hz
    );

  clock_370hz : ENTITY work.son_clock_370hz(Behavioral)
    PORT MAP(
      clk          => clk,
      rst          => rst,
      etat_son     => etat_son,
      signal_370hz => signal_370hz
    );

  clock_415hz : ENTITY work.son_clock_415hz(Behavioral)
    PORT MAP(
      clk          => clk,
      rst          => rst,
      etat_son     => etat_son,
      signal_415hz => signal_415hz
    );

  clock_524hz : ENTITY work.son_clock_524hz(Behavioral)
    PORT MAP(
      clk          => clk,
      rst          => rst,
      etat_son     => etat_son,
      signal_524hz => signal_524hz
    );

  clock_441hz : ENTITY work.son_clock_441hz(Behavioral)
    PORT MAP(
      clk          => clk,
      rst          => rst,
      etat_son     => etat_son,
      signal_441hz => signal_441hz
    );

  clock_262hz : ENTITY work.son_clock_262hz(Behavioral)
    PORT MAP(
      clk          => clk,
      rst          => rst,
      etat_son     => etat_son,
      signal_262hz => signal_262hz
    );

END Behavioral;