----------------------------------------------------------------------------------
-- Company:
-- Engineer: Erwan Sturzenegger
--
-- Create Date:    09:31:05 05/12/2021
-- Design Name:  Projet_integre
-- Module Name:    verification_entree - Behavioral
-- Project Name: Projet intégré 2021 - Groupe 5
-- Target Devices:
-- Tool versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------
LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;
ENTITY verification_entrees IS
	PORT
	(
		clk         : IN STD_LOGIC;
		rst         : STD_LOGIC;
		sequence    : IN  STD_LOGIC_VECTOR (47 DOWNTO 0);
		btn_presse  : IN  STD_LOGIC_VECTOR (5 DOWNTO 0);
		etat_jeu    : IN  STD_LOGIC_VECTOR (2 DOWNTO 0);
		no_sequence : IN  STD_LOGIC_VECTOR (4 DOWNTO 0);
		gagne       : OUT STD_LOGIC;
		perdu       : OUT STD_LOGIC;
		btn_relache : IN  STD_LOGIC_VECTOR (5 DOWNTO 0)
	);
END verification_entrees;

ARCHITECTURE Behavioral OF verification_entrees IS
	TYPE etats_verif IS (ver_perdu, ver_gagne, ver_null);
	SIGNAL etat_verif_present, etat_verif_futur : etats_verif;
	SIGNAL btn_present, btn_futur               : STD_LOGIC_VECTOR (5 DOWNTO 0);
	SIGNAL sequence_presente, sequence_futur    : STD_LOGIC_VECTOR (47 DOWNTO 0) := (OTHERS => '0');
	SIGNAL index_present, index_futur           : unsigned(4 DOWNTO 0)           := (OTHERS => '0');

BEGIN
	perdu <= '1' WHEN etat_verif_present = ver_perdu ELSE
		'0';
	gagne <= '1' WHEN etat_verif_present = ver_gagne ELSE
		'0';

	PROCESS (clk, rst)
	BEGIN
		IF (rst = '1') THEN
			btn_present        <= (OTHERS => '0');
			sequence_presente  <= (OTHERS => '0');
			index_present      <= (OTHERS => '0');
			etat_verif_present <= ver_null;
		ELSIF rising_edge(clk) THEN
			btn_present        <= btn_futur;
			sequence_presente  <= sequence_futur;
			index_present      <= index_futur;
			etat_verif_present <= etat_verif_futur;
		END IF;
	END PROCESS;

	PROCESS (btn_present, btn_presse, btn_relache, index_present, sequence_presente, no_sequence, sequence, etat_verif_present, etat_jeu)
	BEGIN
		sequence_futur <= sequence_presente;
		index_futur    <= index_present;
		btn_futur      <= btn_present;
		CASE etat_verif_present IS
			WHEN ver_null =>
				IF etat_jeu = "010" THEN
					IF btn_present /= btn_presse AND btn_presse /= "000000" AND btn_present = "000000" THEN
						--Bouton pressé depuis un état non pressé
						btn_futur   <= btn_presse;
						index_futur <= index_present + 1;
						IF (btn_presse(2 DOWNTO 0) = "000") THEN
							sequence_futur                                                                             <= sequence_presente;
							sequence_futur((to_integer(index_present) * 3 + 2) DOWNTO (to_integer(index_present) * 3)) <= btn_presse(5 DOWNTO 3);
						ELSIF (btn_presse(5 DOWNTO 3) = "000") THEN
							sequence_futur                                                                             <= sequence_presente;
							sequence_futur((to_integer(index_present) * 3 + 2) DOWNTO (to_integer(index_present) * 3)) <= btn_presse(2 DOWNTO 0);
						ELSE
							sequence_futur <= sequence_presente;
						END IF;
						etat_verif_futur <= ver_null;
						--Bouton relaché depuis un état pressé
					ELSIF (btn_present = btn_relache OR btn_present = (btn_relache(2 DOWNTO 0)&btn_relache(5 DOWNTO 3))) AND btn_relache /= "000000" AND btn_present /= "000000" THEN
						btn_futur        <= (OTHERS => '0');
						sequence_futur   <= sequence_presente;
						index_futur      <= index_present;
						etat_verif_futur <= ver_null;
						IF (index_present > 0 AND (sequence(to_integer(index_present-1) * 3 + 2 DOWNTO 0) = sequence_presente(to_integer(index_present-1) * 3 + 2 DOWNTO 0))) THEN
							IF (to_integer(index_present) = to_integer(unsigned(no_sequence))) THEN
								etat_verif_futur <= ver_gagne;
							END IF;
						ELSIF index_present > 0 THEN
							etat_verif_futur <= ver_perdu;
						END IF;
						-- Autres cas
					ELSE
						btn_futur        <= btn_present;
						sequence_futur   <= sequence_presente;
						index_futur      <= index_present;
						etat_verif_futur <= etat_verif_present;
					END IF;
				ELSE
					btn_futur    <= (OTHERS => '0');
					sequence_futur <= (OTHERS => '0');
					index_futur  <= (OTHERS => '0');
					etat_verif_futur <= ver_null;
				END IF;
			WHEN OTHERS =>
				IF (etat_jeu /= "010") THEN
					etat_verif_futur <= ver_null;
				ELSE
					etat_verif_futur <= etat_verif_present;
				END IF;
		END CASE;
	END PROCESS;

END Behavioral;