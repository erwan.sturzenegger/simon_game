----------------------------------------------------------------------------------
-- Company: HEIA-FR
-- Engineer: Naël Telfser
-- 
-- Create Date:    11:55:16 05/19/2021 
-- Design Name: 	Simon_Game
-- Module Name:    Son_Clock_252Hz - Behavioral 
-- Project Name: 	TN2 - Projet Inegré - Groupe 5
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Son_Clock_252Hz is
    Port ( clk : in  STD_LOGIC;
           rst : in  STD_LOGIC;
           etat_son : in  STD_LOGIC_VECTOR (3 downto 0);
           signal_252hz : out  STD_LOGIC);
end Son_Clock_252Hz;

architecture Behavioral of Son_Clock_252Hz is
	type etats is (bas, haut);

	signal etat_present_252hz, etat_futur_252hz : etats;
	signal compteur_present_252hz, compteur_futur_252hz : Unsigned(6 downto 0) := (others => '0');
	constant half_period_252hz : Unsigned(6 downto 0) := to_unsigned(126, 7);
	signal compte_252hz, fin_comptage_252hz : std_logic;
begin
	compte_252hz <= '1' when etat_son = "0011" else '0';
	fin_comptage_252hz <= '1' when compteur_present_252hz = half_period_252hz else '0';

	combi_252hz : process(etat_present_252hz, compte_252hz, fin_comptage_252hz)
	begin
		case( etat_present_252hz ) is
		
			when bas =>
				if fin_comptage_252hz = '1' then
					etat_futur_252hz <= haut;
				else
					etat_futur_252hz <= bas;
				end if ;
			when haut =>
				if compte_252hz = '0' or fin_comptage_252hz = '1' then
					etat_futur_252hz <= bas;
				else
					etat_futur_252hz <= haut;
				end if ;
			when others => etat_futur_252hz <= bas;
		end case ;
	end process;

	registre_252hz: process(clk, rst)
	begin
		if rst = '1' then
			etat_present_252hz <= bas;
		elsif rising_edge(clk) then
			etat_present_252hz <= etat_futur_252hz;
		end if ;
	end process;

	registre_compteur_252hz: process(clk, rst)
	begin
		if rst = '1' then
			compteur_present_252hz <= (others => '0');
		elsif rising_edge(clk) then
			compteur_present_252hz <= compteur_futur_252hz;
		end if ;
	end process;

	combi_compteur_252hz: process(compteur_present_252hz, compte_252hz, fin_comptage_252hz)
	begin
		if compte_252hz = '0' or fin_comptage_252hz = '1' then
			compteur_futur_252hz <= (others => '0');
		else
			compteur_futur_252hz <= compteur_present_252hz + 1;
		end if ;
	end process;

	signal_252hz <= '1' when etat_present_252hz = haut else '0';
end Behavioral;

