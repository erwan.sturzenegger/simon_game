force -freeze sim:/son_clock_262hz/clk 1 0, 0 {7812500 ps} -r {15625 ns}
# Test état 265hz
force -freeze sim:/son_clock_262hz/rst 0 0
force -freeze sim:/son_clock_262hz/etat_son 1001 0
run 1000 ms
# Test reset
force -freeze sim:/son_clock_262hz/rst 1 0
force -freeze sim:/son_clock_262hz/etat_son 0000 0
run 1000 ms
# Test état 265hz
force -freeze sim:/son_clock_262hz/rst 0 0
force -freeze sim:/son_clock_262hz/etat_son 1001 0
run 1000 ms
# Test autre état
force -freeze sim:/son_clock_262hz/rst 0 0
force -freeze sim:/son_clock_262hz/etat_son 0000 0
run 1000 ms