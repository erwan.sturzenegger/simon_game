library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity Generateur_Sequence is
    Port ( clk: in STD_LOGIC;
           rst: in STD_LOGIC;
           etat_jeu : in  STD_LOGIC_VECTOR (2 downto 0);
           sequence : out  STD_LOGIC_VECTOR (47 downto 0);
           fin_generation_sequence : out  STD_LOGIC);
end Generateur_Sequence;

architecture Behavioral of Generateur_Sequence is

  SIGNAL lfsr_futur, lfsr_present, lsfr_mem: std_logic_vector (47 DOWNTO 0);
  type etats is (rien, memorise, corrige_valeurs, fin);
  signal etat_present, etat_futur : etats;
  signal fin_corrige : std_logic;
  signal index : unsigned(5 downto 0);

BEGIN
  sequence <=  lsfr_mem;
  fin_generation_sequence <= '1' when etat_present = fin else '0';

  registre: PROCESS (clk, rst)
  BEGIN
    IF rst = '1' THEN
      lfsr_present <= "110111001001011100111011100011010111100010000101";
    ELSIF rising_edge(clk) THEN
      lfsr_present <= lfsr_futur;
    END IF;
  END PROCESS;

  calcul_lfsr:process(lfsr_present)
  begin
    lfsr_futur(47 downto 1) <= lfsr_present(46 downto 0);
    lfsr_futur(0)           <= lfsr_present(47);

    lfsr_futur(41) <= lfsr_present(40) xor lfsr_present(47);
    lfsr_futur(28) <= lfsr_present(27) xor lfsr_present(47);
    lfsr_futur(20) <= lfsr_present(19) xor lfsr_present(47);
    lfsr_futur(13) <= lfsr_present(12) xor lfsr_present(47);
    lfsr_futur(6)  <= lfsr_present(5) xor lfsr_present(47);
  end process calcul_lfsr;


  registre_machine_etats: PROCESS (clk, rst)
  BEGIN
    IF rst = '1' THEN
      etat_present <= rien;
    ELSIF rising_edge(clk) THEN
      etat_present <= etat_futur;
    END IF;
  END PROCESS;

  calcul_etat_futur: process(etat_present, etat_jeu, fin_corrige)
  BEGIN
    case etat_present is
      when rien => if etat_jeu="101" THEN
                     etat_futur <= memorise;
                   else
                     etat_futur <= rien;
                   end if;

      when memorise => etat_futur <= corrige_valeurs;

      when corrige_valeurs => if fin_corrige = '1' THEN
                                etat_futur <= fin;
                              else
                                etat_futur <= corrige_valeurs;
                              end if;

      when fin => etat_futur <= rien;
      when others => etat_futur <= rien;
    end case;
  end process;

  fin_corrige <= '1' when index >= "101101" else '0';

  compteur_index: PROCESS (clk, rst)
  BEGIN
    IF rst = '1' THEN
      index <= (others => '0');
    ELSIF rising_edge(clk) THEN
      if etat_present = corrige_valeurs THEN
        if fin_corrige = '1' THEN
          index <= (others => '0');
        else
          index <= index + 3;
        end if;
      end if;
    END IF;
  END PROCESS;

  mem: PROCESS (clk, rst)
  BEGIN
    IF rst = '1' THEN
      lsfr_mem <= (others => '0');
    ELSIF rising_edge(clk) THEN
      if etat_present = memorise THEN
        lsfr_mem <= lfsr_present;
      elsif etat_present = corrige_valeurs THEN
        if lsfr_mem(to_integer(index+2) downto to_integer(index)) = "000" THEN
          lsfr_mem(to_integer(index+2) downto to_integer(index)) <= "100";
        elsif lsfr_mem(to_integer(index+2) downto to_integer(index)) = "110" THEN
          lsfr_mem(to_integer(index+2) downto to_integer(index)) <= "001";
        elsif lsfr_mem(to_integer(index+2) downto to_integer(index)) = "111" THEN
            lsfr_mem(to_integer(index+2) downto to_integer(index)) <= "011";
        end if;
      end if;
    END IF;
  END PROCESS;

end Behavioral;
