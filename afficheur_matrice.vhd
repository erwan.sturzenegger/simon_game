----------------------------------------------------------------------------------
-- Company:
-- Engineer: Erwan Sturzenegger
--
-- Create Date:    09:31:05 05/12/2021
-- Design Name:  Projet_integre
-- Module Name:    afficheur_matrice - Behavioral
-- Project Name: Projet intégré 2021 - Groupe 5
-- Target Devices:
-- Tool versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------
LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_ARITH.ALL;
USE IEEE.STD_LOGIC_UNSIGNED.ALL;

ENTITY afficheur_matrice IS
  PORT
  (
    clk        : IN  STD_LOGIC;
    rst        : IN  STD_LOGIC;
    etat_jeu   : IN  STD_LOGIC_VECTOR (2 DOWNTO 0);
    btn_presse : IN  STD_LOGIC_VECTOR (5 DOWNTO 0);
    symbole    : IN  STD_LOGIC_VECTOR (2 DOWNTO 0);
    ligne      : OUT STD_LOGIC_VECTOR (7 DOWNTO 0);
    colonne    : OUT STD_LOGIC_VECTOR (7 DOWNTO 0));
END afficheur_matrice;

ARCHITECTURE Behavioral OF afficheur_matrice IS
  TYPE type_symbole IS ARRAY(7 DOWNTO 0) OF STD_LOGIC_VECTOR(7 DOWNTO 0);
  CONSTANT haut         : type_symbole                  := ("00011000", "00111100", "01111110", "11111111", "00011000", "00011000", "00011000", "00011000");
  CONSTANT gauche       : type_symbole                  := ("00010000", "00110000", "01110000", "11111111", "11111111", "01110000", "00110000", "00010000");
  CONSTANT droit        : type_symbole                  := ("00001000", "00001100", "00001110", "11111111", "11111111", "00001110", "00001100", "00001000");
  CONSTANT bas          : type_symbole                  := ("00011000", "00011000", "00011000", "00011000", "11111111", "01111110", "00111100", "00011000");
  CONSTANT centre       : type_symbole                  := ("11111111", "11111111", "11000011", "11011011", "11011011", "11000011", "11111111", "11111111");
  CONSTANT gagne        : type_symbole                  := ("00111100", "01000010", "10100101", "10000001", "10100101", "10011001", "01000010", "00111100");
  CONSTANT perdu        : type_symbole                  := ("00111100", "01000010", "10100101", "10000001", "10011001", "10100101", "01000010", "00111100");
  CONSTANT vide         : type_symbole                  := ("00000000", "00000000", "00000000", "00000000", "00000000", "00000000", "00000000", "00000000");
  SIGNAL afficher_ligne : STD_LOGIC_VECTOR (7 DOWNTO 0) := "11111110";
  SIGNAL counter        : STD_LOGIC_VECTOR(5 DOWNTO 0)  := (OTHERS => '0');
  SIGNAL symbole_actuel : type_symbole                  := vide;
BEGIN
  matrix : PROCESS (clk, rst)
  BEGIN
    IF rst = '1' THEN
      counter <= "000000";
    ELSIF rising_edge(clk) THEN
      counter <= counter + 1;
      IF counter = 0 THEN
        afficher_ligne <= afficher_ligne(6 DOWNTO 0) & afficher_ligne(7);
      END IF;
      boucle_recherche : FOR i IN 0 TO 7 LOOP
        IF (afficher_ligne(i) = '1') THEN
          colonne <= symbole_actuel(i);
        END IF;
      END LOOP boucle_recherche;
      ligne <= afficher_ligne;
    END IF;
  END PROCESS matrix;

  control : PROCESS (etat_jeu, btn_presse, symbole)
  BEGIN
    CASE etat_jeu IS
      WHEN "010" =>
        IF btn_presse = "001000" THEN
          symbole_actuel <= haut;
        ELSIF btn_presse = "010000" THEN
          symbole_actuel <= bas;
        ELSIF btn_presse = "011000" THEN
          symbole_actuel <= gauche;
        ELSIF btn_presse = "100000" THEN
          symbole_actuel <= droit;
        ELSIF btn_presse = "101000" THEN
          symbole_actuel <= centre;
        ELSE
          symbole_actuel <= vide;
        END IF;

      WHEN "001" | "011" | "100" =>
        CASE symbole IS
          WHEN "001"  => symbole_actuel  <= haut;
          WHEN "010"  => symbole_actuel  <= bas;
          WHEN "011"  => symbole_actuel  <= gauche;
          WHEN "100"  => symbole_actuel  <= droit;
          WHEN "101"  => symbole_actuel  <= centre;
          WHEN "110"  => symbole_actuel  <= gagne;
          WHEN "111"  => symbole_actuel  <= perdu;
          WHEN OTHERS => symbole_actuel <= vide;
        END CASE;
      WHEN OTHERS => symbole_actuel <= vide;
    END CASE;
  END PROCESS control;
END Behavioral;