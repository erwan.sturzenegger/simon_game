----------------------------------------------------------------------------------
-- Company: HEIA-FR
-- Engineer: Naël Telfser
-- 
-- Create Date:    11:56:07 05/19/2021 
-- Design Name: 	Simon_Game
-- Module Name:    Son_Clock_310Hz - Behavioral 
-- Project Name: 	TN2 - Projet Inegré - Groupe 5
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Son_Clock_310Hz is
    Port ( clk : in  STD_LOGIC;
           rst : in  STD_LOGIC;
           etat_son : in  STD_LOGIC_VECTOR (3 downto 0);
           signal_310hz : out  STD_LOGIC);
end Son_Clock_310Hz;

architecture Behavioral of Son_Clock_310Hz is
	type etats is (bas, haut);

	signal etat_present_310hz, etat_futur_310hz : etats;
	signal compteur_present_310hz, compteur_futur_310hz : Unsigned(6 downto 0) := (others => '0');
	constant half_period_310hz : Unsigned(6 downto 0) := to_unsigned(103, 7);
	signal compte_310hz, fin_comptage_310hz : std_logic;
begin
	compte_310hz <= '1' when etat_son = "0100" else '0';
	fin_comptage_310hz <= '1' when compteur_present_310hz = half_period_310hz else '0';

	combi_310hz : process(etat_present_310hz, compte_310hz, fin_comptage_310hz)
	begin
		case( etat_present_310hz ) is
		
			when bas =>
				if fin_comptage_310hz = '1' then
					etat_futur_310hz <= haut;
				else
					etat_futur_310hz <= bas;
				end if ;
			when haut =>
				if compte_310hz = '0' or fin_comptage_310hz = '1' then
					etat_futur_310hz <= bas;
				else
					etat_futur_310hz <= haut;
				end if ;
			when others => etat_futur_310hz <= bas;
		end case ;
	end process;

	registre_310hz: process(clk, rst)
	begin
		if rst = '1' then
			etat_present_310hz <= bas;
		elsif rising_edge(clk) then
			etat_present_310hz <= etat_futur_310hz;
		end if ;
	end process;

	registre_compteur_310hz: process(clk, rst)
	begin
		if rst = '1' then
			compteur_present_310hz <= (others => '0');
		elsif rising_edge(clk) then
			compteur_present_310hz <= compteur_futur_310hz;
		end if ;
	end process;

	combi_compteur_310hz: process(compteur_present_310hz, compte_310hz, fin_comptage_310hz)
	begin
		if compte_310hz = '0' or fin_comptage_310hz = '1' then
			compteur_futur_310hz <= (others => '0');
		else
			compteur_futur_310hz <= compteur_present_310hz + 1;
		end if ;
	end process;
	signal_310hz <= '1' when etat_present_310hz = haut else '0';
end Behavioral;

