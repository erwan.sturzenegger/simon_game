----------------------------------------------------------------------------------
-- Module Name:    GenerateurSymboles - Behavioral
----------------------------------------------------------------------------------
LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE ieee.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
USE IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

ENTITY GenerateurSymboles IS
  PORT (
    clk          : IN  STD_LOGIC;
    rst          : IN  STD_LOGIC;
    no_sequence  : IN  STD_LOGIC_VECTOR (4 DOWNTO 0);
    etat_jeu     : IN  STD_LOGIC_VECTOR (2 DOWNTO 0);
    sequence     : IN  STD_LOGIC_VECTOR (47 DOWNTO 0);
    debug : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
    symbole      : OUT STD_LOGIC_VECTOR (2 DOWNTO 0);
    fin_sequence : OUT STD_LOGIC);
END GenerateurSymboles;

ARCHITECTURE Behavioral OF GenerateurSymboles IS
  SIGNAL symbole_present, symbole_futur : STD_LOGIC_VECTOR(2 DOWNTO 0);
  SIGNAL sequence_index_present, sequence_index_futur : unsigned(5 DOWNTO 0);
  SIGNAL etat_jeu_present, etat_jeu_futur : STD_LOGIC_VECTOR(2 DOWNTO 0);
  SIGNAL fin_sequence_present, fin_sequence_futur : STD_LOGIC;
  TYPE etat IS (AFFICHE_SYMBOLE, AFFICHE_RIEN);
  SIGNAL etat_present, etat_futur : etat;
  --CLOCK--
  CONSTANT seconde : unsigned(15 DOWNTO 0) := to_unsigned(64000, 16);
  CONSTANT end_game_time : unsigned(17 DOWNTO 0) := to_unsigned(136170, 18);
  CONSTANT demi_seconde : unsigned(14 DOWNTO 0) := to_unsigned(32000, 15);
  SIGNAL signal_affichage : STD_LOGIC;
  SIGNAL fin_comptage_1hz, fin_comptage_2hz, fin_comptage_end_game : STD_LOGIC;
  TYPE etat_h IS (HAUT, BAS, END_GAME);
  SIGNAL etat_h_present, etat_h_futur : etat_h;
  SIGNAL compteur_present, compteur_futur : unsigned(17 DOWNTO 0) := (OTHERS => '0');
  SIGNAL fin_jeu_present, fin_jeu_futur : STD_LOGIC;
BEGIN

  fin_sequence <= fin_sequence_present;
  symbole <= symbole_present;
  

  registre : PROCESS (clk, rst)
  BEGIN
    IF rst = '1' THEN
      symbole_present <= "000";
      fin_sequence_present <= '0';
      sequence_index_present <= "000001";
      etat_present <= AFFICHE_RIEN;
      fin_jeu_present <= '0';
    ELSIF rising_edge(clk) THEN
      symbole_present <= symbole_futur;
      fin_sequence_present <= fin_sequence_futur;
      sequence_index_present <= sequence_index_futur;
      etat_present <= etat_futur;
      fin_jeu_present <= fin_jeu_futur;
    END IF;
  END PROCESS;

  combi : PROCESS (etat_jeu, signal_affichage, no_sequence, sequence, fin_sequence_present, sequence_index_present, etat_present, symbole_present, fin_jeu_present)
  BEGIN
    CASE (etat_jeu) IS
      WHEN "001" =>
      fin_jeu_futur <= '0';
        CASE etat_present IS
          WHEN AFFICHE_SYMBOLE =>
            IF (signal_affichage = '1') THEN
              IF ((sequence_index_present - 1) > unsigned(no_sequence)) THEN
                etat_futur <= AFFICHE_RIEN;
                sequence_index_futur <= "000001";
                symbole_futur <= "000";
                fin_sequence_futur <= '1';
              ELSE
                fin_sequence_futur <= '0';
                etat_futur <= AFFICHE_SYMBOLE;
                symbole_futur <= symbole_present;
                sequence_index_futur <= sequence_index_present;
              END IF;
            ELSE
              etat_futur <= AFFICHE_RIEN;
              IF ((sequence_index_present - 1) > unsigned(no_sequence)) THEN
                fin_sequence_futur <= '1';
              ELSE
                fin_sequence_futur <= '0';
              END IF;
              sequence_index_futur <= sequence_index_present;
              symbole_futur <= "000";
            END IF;
          WHEN AFFICHE_RIEN =>
            IF (fin_sequence_present = '1') THEN
              etat_futur <= AFFICHE_RIEN;
              sequence_index_futur <= "000001";
              symbole_futur <= "000";
              fin_sequence_futur <= '1';
            ELSE
              IF (signal_affichage = '0') THEN
                etat_futur <= AFFICHE_RIEN;
                sequence_index_futur <= sequence_index_present;
                symbole_futur <= "000";
                fin_sequence_futur <= '0';
              ELSE
                fin_sequence_futur <= '0';
                etat_futur <= AFFICHE_SYMBOLE;
                if(sequence_index_present <= to_unsigned(16, 5)) then
                  symbole_futur <= sequence((to_integer(sequence_index_present-1) * 3 + 2) DOWNTO (to_integer(sequence_index_present-1) * 3));
                else 
                  symbole_futur <= "000";
                end if;
                sequence_index_futur <= sequence_index_present + 1;
              END IF;
            END IF;
          WHEN OTHERS =>
            fin_sequence_futur <= '0';
            etat_futur <= AFFICHE_RIEN;
            symbole_futur <= "000";
            sequence_index_futur <= "000001";
        END CASE;
        --OK
      WHEN "011" =>
        IF signal_affichage = '1' THEN
          symbole_futur <= "110";
          etat_futur <= AFFICHE_SYMBOLE;
          sequence_index_futur <= sequence_index_present;
          fin_sequence_futur <= '0';
          fin_jeu_futur <= '1';
        elsif(fin_jeu_present = '1') then
          symbole_futur <= "000";
          etat_futur <= AFFICHE_RIEN;
          sequence_index_futur <= sequence_index_present;
          fin_sequence_futur <= '1';
          fin_jeu_futur <= '1';
        ELSE
          symbole_futur <= "000";
          etat_futur <= AFFICHE_RIEN;
          sequence_index_futur <= sequence_index_present;
          fin_sequence_futur <= '0';
          fin_jeu_futur <= '0';
        END IF;
      WHEN "100" =>
        IF signal_affichage = '1' THEN
          symbole_futur <= "111";
          etat_futur <= AFFICHE_SYMBOLE;
          sequence_index_futur <= sequence_index_present;
          fin_sequence_futur <= '0';
          fin_jeu_futur <= '1';
        elsif(fin_jeu_present = '1') then
          symbole_futur <= "000";
          etat_futur <= AFFICHE_RIEN;
          sequence_index_futur <= sequence_index_present;
          fin_sequence_futur <= '1';
          fin_jeu_futur <= '1';
        ELSE
          symbole_futur <= "000";
          etat_futur <= AFFICHE_RIEN;
          sequence_index_futur <= sequence_index_present;
          fin_sequence_futur <= '0';
          fin_jeu_futur <= '0';
        END IF;
      WHEN OTHERS =>
        symbole_futur <= "000";
        sequence_index_futur <= "000001";
        etat_futur <= AFFICHE_RIEN;
        fin_sequence_futur <= '0';
    END CASE;
  END PROCESS;

  --CLOCK--
  fin_comptage_1hz <= '1' WHEN compteur_present = seconde ELSE
    '0';
  fin_comptage_2hz <= '1' WHEN compteur_present = demi_seconde ELSE
    '0';
  fin_comptage_end_game <= '1' WHEN compteur_present = end_game_time ELSE
    '0';
  signal_affichage <= '1' WHEN (etat_h_present = HAUT AND (etat_jeu_present = "001" OR etat_jeu_present = "100" OR etat_jeu_present = "011")) OR etat_h_present = END_GAME ELSE
    '0';

  PROCESS (clk, rst)
  BEGIN
    IF (rst = '1') THEN
      etat_h_present <= BAS;
      compteur_present <= (OTHERS => '0');
      etat_jeu_present <= "000";
    ELSIF (rising_edge(clk)) THEN
      etat_h_present <= etat_h_futur;
      compteur_present <= compteur_futur;
      etat_jeu_present <= etat_jeu_futur;
    END IF;
  END PROCESS;

  PROCESS (etat_h_present, fin_comptage_1hz, fin_comptage_2hz, fin_comptage_end_game, compteur_present, etat_jeu, etat_jeu_present)
  BEGIN
    IF (etat_jeu /= etat_jeu_present) THEN
      etat_jeu_futur <= etat_jeu;
      etat_h_futur <= BAS;
      compteur_futur <= (OTHERS => '0');
    ELSE
      etat_jeu_futur <= etat_jeu_present;
      CASE etat_h_present IS
        WHEN HAUT =>
          IF (fin_comptage_1hz = '1') THEN
            etat_h_futur <= BAS;
            compteur_futur <= (OTHERS => '0');
          ELSE
            etat_h_futur <= HAUT;
            compteur_futur <= compteur_present + 1;
          END IF;
        WHEN BAS =>
          IF (fin_comptage_2hz = '1') THEN
            if(etat_jeu = "100" OR etat_jeu = "011") then
              etat_h_futur <= END_GAME;
            else 
              etat_h_futur <= HAUT;
            end if;
            compteur_futur <= (OTHERS => '0');
          ELSE
            etat_h_futur <= BAS;
            compteur_futur <= compteur_present + 1;
          END IF;
        WHEN END_GAME =>
        if(fin_comptage_end_game = '1') then
          etat_h_futur <= BAS;
          compteur_futur <= (OTHERS => '0');
        else
          etat_h_futur <= END_GAME;
          compteur_futur <= compteur_present + 1;
        end if;
        WHEN OTHERS => etat_h_futur <= BAS;
      END CASE;
    END IF;
  END PROCESS;
END Behavioral;