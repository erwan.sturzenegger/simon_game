----------------------------------------------------------------------------------
-- Company: HEIA-FR
-- Engineer: Naël Telfser
-- 
-- Create Date:    12:00:19 05/19/2021 
-- Design Name: 	Simon_Game
-- Module Name:    Son_Clock_3_3Hz - Behavioral 
-- Project Name: 	TN2 - Projet Inegré - Groupe 5
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Son_Clock_3_3Hz is
    Port ( clk : in  STD_LOGIC;
           rst : in  STD_LOGIC;
           etat_son : in  STD_LOGIC_VECTOR (3 downto 0);
           signal_3_3hz : out  STD_LOGIC);
end Son_Clock_3_3Hz;

architecture Behavioral of Son_Clock_3_3Hz is
	type etats is (bas, haut);
	signal etat_present_3_3hz, etat_futur_3_3hz : etats;
	signal compteur_present_3_3hz, compteur_futur_3_3hz : Unsigned(13 downto 0) := (others => '0');
	constant half_period_3_3hz : Unsigned(13 downto 0) := to_unsigned(9696, 14);
	signal compte_3_3hz, fin_comptage_3_3hz : std_logic := '0';

begin
	compte_3_3hz <= '1' when etat_son = "0001" else '0';
	fin_comptage_3_3hz <= '1' when compteur_present_3_3hz = half_period_3_3hz else '0';

	combi_3_3hz : process(etat_present_3_3hz, compte_3_3hz, fin_comptage_3_3hz)
	begin
		case( etat_present_3_3hz ) is
		
			when bas =>
				if fin_comptage_3_3hz = '1' then
					etat_futur_3_3hz <= haut;
				else
					etat_futur_3_3hz <= bas;
				end if ;
			when haut =>
				if compte_3_3hz = '0' or fin_comptage_3_3hz = '1' then
					etat_futur_3_3hz <= bas;
				else
					etat_futur_3_3hz <= haut;
				end if ;
			when others => etat_futur_3_3hz <= bas;
		end case ;
	end process;

	registre_3_3hz: process(clk, rst)
	begin
		if rst = '1' then
			etat_present_3_3hz <= bas;
		elsif rising_edge(clk) then
			etat_present_3_3hz <= etat_futur_3_3hz;
		end if ;
	end process;

	registre_compteur_3_3hz: process(clk, rst)
	begin
		if rst = '1' then
			compteur_present_3_3hz <= (others => '0');
		elsif rising_edge(clk) then
			compteur_present_3_3hz <= compteur_futur_3_3hz;
		end if ;
	end process;

	combi_compteur_3_3hz: process(compteur_present_3_3hz, compte_3_3hz, fin_comptage_3_3hz)
	begin
		if compte_3_3hz = '0' or fin_comptage_3_3hz = '1' then
			compteur_futur_3_3hz <= (others => '0');
		else
			compteur_futur_3_3hz <= compteur_present_3_3hz + 1;
		end if ;
	end process;

	signal_3_3hz <= '1' when etat_present_3_3hz = haut else '0';

end Behavioral;

