force -freeze sim:/generateur_son/clk 1 0, 0 {7812500 ps} -r {15625 ns}

# Reset

force -freeze sim:/generateur_son/rst 1 0
force -freeze sim:/generateur_son/etat_jeu 000 0
force -freeze sim:/generateur_son/symbole 000 0
force -freeze sim:/generateur_son/signal_2hz 0 0
force -freeze sim:/generateur_son/signal_3_3hz 0 0
force -freeze sim:/generateur_son/btn_presse 000000 0
run 1000 ms

# Symbole Haut

force -freeze sim:/generateur_son/rst 0 0
force -freeze sim:/generateur_son/etat_jeu 001 0
force -freeze sim:/generateur_son/symbole 001 0
force -freeze sim:/generateur_son/signal_2hz 0 0
force -freeze sim:/generateur_son/signal_3_3hz 0 0
force -freeze sim:/generateur_son/btn_presse 000000 0
run 1000 ms

# Symbole Bas

force -freeze sim:/generateur_son/rst 0 0
force -freeze sim:/generateur_son/etat_jeu 001 0
force -freeze sim:/generateur_son/symbole 010 0
force -freeze sim:/generateur_son/signal_2hz 0 0
force -freeze sim:/generateur_son/signal_3_3hz 0 0
force -freeze sim:/generateur_son/btn_presse 000000 0
run 1000 ms

# Symbole Gauche

force -freeze sim:/generateur_son/rst 0 0
force -freeze sim:/generateur_son/etat_jeu 001 0
force -freeze sim:/generateur_son/symbole 011 0
force -freeze sim:/generateur_son/signal_2hz 0 0
force -freeze sim:/generateur_son/signal_3_3hz 0 0
force -freeze sim:/generateur_son/btn_presse 000000 0
run 1000 ms

# Symbole Droite

force -freeze sim:/generateur_son/rst 0 0
force -freeze sim:/generateur_son/etat_jeu 001 0
force -freeze sim:/generateur_son/symbole 100 0
force -freeze sim:/generateur_son/signal_2hz 0 0
force -freeze sim:/generateur_son/signal_3_3hz 0 0
force -freeze sim:/generateur_son/btn_presse 000000 0
run 1000 ms

# Symbole Centre

force -freeze sim:/generateur_son/rst 0 0
force -freeze sim:/generateur_son/etat_jeu 001 0
force -freeze sim:/generateur_son/symbole 101 0
force -freeze sim:/generateur_son/signal_2hz 0 0
force -freeze sim:/generateur_son/signal_3_3hz 0 0
force -freeze sim:/generateur_son/btn_presse 000000 0
run 1000 ms

# Symbole Gagne

force -freeze sim:/generateur_son/rst 0 0
force -freeze sim:/generateur_son/etat_jeu 001 0
force -freeze sim:/generateur_son/symbole 110 0
force -freeze sim:/generateur_son/signal_2hz 0 0
force -freeze sim:/generateur_son/signal_3_3hz 0 0
force -freeze sim:/generateur_son/btn_presse 000000 0
run 1000 ms

# Bouton Haut

force -freeze sim:/generateur_son/rst 0 0
force -freeze sim:/generateur_son/etat_jeu 010 0
force -freeze sim:/generateur_son/symbole 000 0
force -freeze sim:/generateur_son/signal_2hz 0 0
force -freeze sim:/generateur_son/signal_3_3hz 0 0
force -freeze sim:/generateur_son/btn_presse 001000 0
run 1000 ms

# Bouton Bas

force -freeze sim:/generateur_son/rst 0 0
force -freeze sim:/generateur_son/etat_jeu 010 0
force -freeze sim:/generateur_son/symbole 000 0
force -freeze sim:/generateur_son/signal_2hz 0 0
force -freeze sim:/generateur_son/signal_3_3hz 0 0
force -freeze sim:/generateur_son/btn_presse 010000 0
run 1000 ms

# Bouton Gauche

force -freeze sim:/generateur_son/rst 0 0
force -freeze sim:/generateur_son/etat_jeu 010 0
force -freeze sim:/generateur_son/symbole 000 0
force -freeze sim:/generateur_son/signal_2hz 0 0
force -freeze sim:/generateur_son/signal_3_3hz 0 0
force -freeze sim:/generateur_son/btn_presse 011000 0
run 1000 ms

# Bouton Droite

force -freeze sim:/generateur_son/rst 0 0
force -freeze sim:/generateur_son/etat_jeu 010 0
force -freeze sim:/generateur_son/symbole 000 0
force -freeze sim:/generateur_son/signal_2hz 0 0
force -freeze sim:/generateur_son/signal_3_3hz 0 0
force -freeze sim:/generateur_son/btn_presse 100000 0
run 1000 ms

# Bouton Centre

force -freeze sim:/generateur_son/rst 0 0
force -freeze sim:/generateur_son/etat_jeu 010 0
force -freeze sim:/generateur_son/symbole 000 0
force -freeze sim:/generateur_son/signal_2hz 0 0
force -freeze sim:/generateur_son/signal_3_3hz 0 0
force -freeze sim:/generateur_son/btn_presse 101000 0
run 1000 ms

# Deux Bouton

force -freeze sim:/generateur_son/rst 0 0
force -freeze sim:/generateur_son/etat_jeu 010 0
force -freeze sim:/generateur_son/symbole 000 0
force -freeze sim:/generateur_son/signal_2hz 0 0
force -freeze sim:/generateur_son/signal_3_3hz 0 0
force -freeze sim:/generateur_son/btn_presse 101001 0
run 1000 ms


# Gagné 2hz bas

force -freeze sim:/generateur_son/rst 0 0
force -freeze sim:/generateur_son/etat_jeu 011 0
force -freeze sim:/generateur_son/symbole 110 0
force -freeze sim:/generateur_son/signal_2hz 0 0
force -freeze sim:/generateur_son/signal_3_3hz 0 0
force -freeze sim:/generateur_son/btn_presse 000000 0
run 250 ms

# Gagné 2hz haut

force -freeze sim:/generateur_son/rst 0 0
force -freeze sim:/generateur_son/etat_jeu 011 0
force -freeze sim:/generateur_son/symbole 110 0
force -freeze sim:/generateur_son/signal_2hz 1 0
force -freeze sim:/generateur_son/signal_3_3hz 0 0
force -freeze sim:/generateur_son/btn_presse 000000 0
run 250 ms

# Gagné 2hz bas

force -freeze sim:/generateur_son/rst 0 0
force -freeze sim:/generateur_son/etat_jeu 011 0
force -freeze sim:/generateur_son/symbole 110 0
force -freeze sim:/generateur_son/signal_2hz 0 0
force -freeze sim:/generateur_son/btn_presse 000000 0
run 1 ns

# Gagné 3.3hz bas

force -freeze sim:/generateur_son/rst 0 0
force -freeze sim:/generateur_son/etat_jeu 011 0
force -freeze sim:/generateur_son/symbole 110 0
force -freeze sim:/generateur_son/signal_2hz 0 0
force -freeze sim:/generateur_son/signal_3_3hz 0 0
force -freeze sim:/generateur_son/btn_presse 000000 0
run 150 ms

# Gagné 3.3hz haut

force -freeze sim:/generateur_son/rst 0 0
force -freeze sim:/generateur_son/etat_jeu 011 0
force -freeze sim:/generateur_son/symbole 110 0
force -freeze sim:/generateur_son/signal_2hz 0 0
force -freeze sim:/generateur_son/signal_3_3hz 1 0
force -freeze sim:/generateur_son/btn_presse 000000 0
run 150 ms

# Gagné 3.3hz bas

force -freeze sim:/generateur_son/rst 0 0
force -freeze sim:/generateur_son/etat_jeu 011 0
force -freeze sim:/generateur_son/symbole 110 0
force -freeze sim:/generateur_son/signal_2hz 0 0
force -freeze sim:/generateur_son/signal_3_3hz 0 0
force -freeze sim:/generateur_son/btn_presse 000000 0
run 1 ms

# Gagné 2hz bas

force -freeze sim:/generateur_son/rst 0 0
force -freeze sim:/generateur_son/etat_jeu 011 0
force -freeze sim:/generateur_son/symbole 110 0
force -freeze sim:/generateur_son/signal_2hz 0 0
force -freeze sim:/generateur_son/signal_3_3hz 0 0
force -freeze sim:/generateur_son/btn_presse 000000 0
run 250 ms

# Gagné 2hz haut

force -freeze sim:/generateur_son/rst 0 0
force -freeze sim:/generateur_son/etat_jeu 011 0
force -freeze sim:/generateur_son/symbole 110 0
force -freeze sim:/generateur_son/signal_2hz 1 0
force -freeze sim:/generateur_son/signal_3_3hz 0 0
force -freeze sim:/generateur_son/btn_presse 000000 0
run 250 ms

# Gagné 2hz bas

force -freeze sim:/generateur_son/rst 0 0
force -freeze sim:/generateur_son/etat_jeu 011 0
force -freeze sim:/generateur_son/symbole 110 0
force -freeze sim:/generateur_son/signal_2hz 0 0
force -freeze sim:/generateur_son/signal_3_3hz 0 0
force -freeze sim:/generateur_son/btn_presse 000000 0
run 1 ns

# Gagné 3.3hz bas

force -freeze sim:/generateur_son/rst 0 0
force -freeze sim:/generateur_son/etat_jeu 011 0
force -freeze sim:/generateur_son/symbole 110 0
force -freeze sim:/generateur_son/signal_2hz 0 0
force -freeze sim:/generateur_son/signal_3_3hz 0 0
force -freeze sim:/generateur_son/btn_presse 000000 0
run 150 ms

# Gagné 3.3hz haut

force -freeze sim:/generateur_son/rst 0 0
force -freeze sim:/generateur_son/etat_jeu 011 0
force -freeze sim:/generateur_son/symbole 110 0
force -freeze sim:/generateur_son/signal_2hz 0 0
force -freeze sim:/generateur_son/signal_3_3hz 1 0
force -freeze sim:/generateur_son/btn_presse 000000 0
run 150 ms

# Gagné 3.3hz bas

force -freeze sim:/generateur_son/rst 0 0
force -freeze sim:/generateur_son/etat_jeu 011 0
force -freeze sim:/generateur_son/symbole 110 0
force -freeze sim:/generateur_son/signal_2hz 0 0
force -freeze sim:/generateur_son/signal_3_3hz 0 0
force -freeze sim:/generateur_son/btn_presse 000000 0
run 1 ms

# Gagné 2hz bas

force -freeze sim:/generateur_son/rst 0 0
force -freeze sim:/generateur_son/etat_jeu 011 0
force -freeze sim:/generateur_son/symbole 110 0
force -freeze sim:/generateur_son/signal_2hz 0 0
force -freeze sim:/generateur_son/signal_3_3hz 0 0
force -freeze sim:/generateur_son/btn_presse 000000 0
run 250 ms

# Gagné 2hz haut

force -freeze sim:/generateur_son/rst 0 0
force -freeze sim:/generateur_son/etat_jeu 011 0
force -freeze sim:/generateur_son/symbole 110 0
force -freeze sim:/generateur_son/signal_2hz 1 0
force -freeze sim:/generateur_son/signal_3_3hz 0 0
force -freeze sim:/generateur_son/btn_presse 000000 0
run 250 ms

# Gagné 2hz bas

force -freeze sim:/generateur_son/rst 0 0
force -freeze sim:/generateur_son/etat_jeu 011 0
force -freeze sim:/generateur_son/symbole 110 0
force -freeze sim:/generateur_son/signal_2hz 0 0
force -freeze sim:/generateur_son/signal_3_3hz 0 0
force -freeze sim:/generateur_son/btn_presse 000000 0
run 250 ns

#Clear state

force -freeze sim:/generateur_son/rst 0 0
force -freeze sim:/generateur_son/etat_jeu 000 0
force -freeze sim:/generateur_son/symbole 000 0
force -freeze sim:/generateur_son/signal_2hz 0 0
force -freeze sim:/generateur_son/signal_3_3hz 0 0
force -freeze sim:/generateur_son/btn_presse 000000 0
run 1000 ms

# Perdu 2hz bas

force -freeze sim:/generateur_son/rst 0 0
force -freeze sim:/generateur_son/etat_jeu 100 0
force -freeze sim:/generateur_son/symbole 111 0
force -freeze sim:/generateur_son/signal_2hz 0 0
force -freeze sim:/generateur_son/signal_3_3hz 0 0
force -freeze sim:/generateur_son/btn_presse 000000 0
run 250 ms

# Perdu 2hz haut

force -freeze sim:/generateur_son/rst 0 0
force -freeze sim:/generateur_son/etat_jeu 100 0
force -freeze sim:/generateur_son/symbole 111 0
force -freeze sim:/generateur_son/signal_2hz 1 0
force -freeze sim:/generateur_son/signal_3_3hz 0 0
force -freeze sim:/generateur_son/btn_presse 000000 0
run 250 ms

# Perdu 2hz bas

force -freeze sim:/generateur_son/rst 0 0
force -freeze sim:/generateur_son/etat_jeu 100 0
force -freeze sim:/generateur_son/symbole 111 0
force -freeze sim:/generateur_son/signal_2hz 0 0
force -freeze sim:/generateur_son/btn_presse 000000 0
run 1 ns

# Perdu 3.3hz bas

force -freeze sim:/generateur_son/rst 0 0
force -freeze sim:/generateur_son/etat_jeu 100 0
force -freeze sim:/generateur_son/symbole 111 0
force -freeze sim:/generateur_son/signal_2hz 0 0
force -freeze sim:/generateur_son/signal_3_3hz 0 0
force -freeze sim:/generateur_son/btn_presse 000000 0
run 150 ms

# Perdu 3.3hz haut

force -freeze sim:/generateur_son/rst 0 0
force -freeze sim:/generateur_son/etat_jeu 100 0
force -freeze sim:/generateur_son/symbole 111 0
force -freeze sim:/generateur_son/signal_2hz 0 0
force -freeze sim:/generateur_son/signal_3_3hz 1 0
force -freeze sim:/generateur_son/btn_presse 000000 0
run 150 ms

# Perdu 3.3hz bas

force -freeze sim:/generateur_son/rst 0 0
force -freeze sim:/generateur_son/etat_jeu 100 0
force -freeze sim:/generateur_son/symbole 111 0
force -freeze sim:/generateur_son/signal_2hz 0 0
force -freeze sim:/generateur_son/signal_3_3hz 0 0
force -freeze sim:/generateur_son/btn_presse 000000 0
run 1 ms

# Perdu 2hz bas

force -freeze sim:/generateur_son/rst 0 0
force -freeze sim:/generateur_son/etat_jeu 100 0
force -freeze sim:/generateur_son/symbole 111 0
force -freeze sim:/generateur_son/signal_2hz 0 0
force -freeze sim:/generateur_son/signal_3_3hz 0 0
force -freeze sim:/generateur_son/btn_presse 000000 0
run 250 ms

# Perdu 2hz haut

force -freeze sim:/generateur_son/rst 0 0
force -freeze sim:/generateur_son/etat_jeu 100 0
force -freeze sim:/generateur_son/symbole 111 0
force -freeze sim:/generateur_son/signal_2hz 1 0
force -freeze sim:/generateur_son/signal_3_3hz 0 0
force -freeze sim:/generateur_son/btn_presse 000000 0
run 250 ms

# Perdu 2hz bas

force -freeze sim:/generateur_son/rst 0 0
force -freeze sim:/generateur_son/etat_jeu 100 0
force -freeze sim:/generateur_son/symbole 111 0
force -freeze sim:/generateur_son/signal_2hz 0 0
force -freeze sim:/generateur_son/signal_3_3hz 0 0
force -freeze sim:/generateur_son/btn_presse 000000 0
run 1 ns

# Perdu 3.3hz bas

force -freeze sim:/generateur_son/rst 0 0
force -freeze sim:/generateur_son/etat_jeu 100 0
force -freeze sim:/generateur_son/symbole 111 0
force -freeze sim:/generateur_son/signal_2hz 0 0
force -freeze sim:/generateur_son/signal_3_3hz 0 0
force -freeze sim:/generateur_son/btn_presse 000000 0
run 150 ms

# Perdu 3.3hz haut

force -freeze sim:/generateur_son/rst 0 0
force -freeze sim:/generateur_son/etat_jeu 100 0
force -freeze sim:/generateur_son/symbole 111 0
force -freeze sim:/generateur_son/signal_2hz 0 0
force -freeze sim:/generateur_son/signal_3_3hz 1 0
force -freeze sim:/generateur_son/btn_presse 000000 0
run 150 ms

# Perdu 3.3hz bas

force -freeze sim:/generateur_son/rst 0 0
force -freeze sim:/generateur_son/etat_jeu 100 0
force -freeze sim:/generateur_son/symbole 111 0
force -freeze sim:/generateur_son/signal_2hz 0 0
force -freeze sim:/generateur_son/signal_3_3hz 0 0
force -freeze sim:/generateur_son/btn_presse 000000 0
run 1 ms

# Perdu 2hz bas

force -freeze sim:/generateur_son/rst 0 0
force -freeze sim:/generateur_son/etat_jeu 100 0
force -freeze sim:/generateur_son/symbole 111 0
force -freeze sim:/generateur_son/signal_2hz 0 0
force -freeze sim:/generateur_son/signal_3_3hz 0 0
force -freeze sim:/generateur_son/btn_presse 000000 0
run 250 ms

# Perdu 2hz haut

force -freeze sim:/generateur_son/rst 0 0
force -freeze sim:/generateur_son/etat_jeu 100 0
force -freeze sim:/generateur_son/symbole 111 0
force -freeze sim:/generateur_son/signal_2hz 1 0
force -freeze sim:/generateur_son/signal_3_3hz 0 0
force -freeze sim:/generateur_son/btn_presse 000000 0
run 250 ms

# Perdu 2hz bas

force -freeze sim:/generateur_son/rst 0 0
force -freeze sim:/generateur_son/etat_jeu 100 0
force -freeze sim:/generateur_son/symbole 111 0
force -freeze sim:/generateur_son/signal_2hz 0 0
force -freeze sim:/generateur_son/signal_3_3hz 0 0
force -freeze sim:/generateur_son/btn_presse 000000 0
run 250 ns

#Clear state

force -freeze sim:/generateur_son/rst 0 0
force -freeze sim:/generateur_son/etat_jeu 000 0
force -freeze sim:/generateur_son/symbole 000 0
force -freeze sim:/generateur_son/signal_2hz 0 0
force -freeze sim:/generateur_son/signal_3_3hz 0 0
force -freeze sim:/generateur_son/btn_presse 000000 0
run 1000 ms