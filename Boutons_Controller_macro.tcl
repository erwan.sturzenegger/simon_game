force -freeze sim:/boutons_controller/clk 1 0, 0 {7812500 ps} -r {15625 ns}
force -freeze sim:/boutons_controller/rst 1 0
force -freeze sim:/boutons_controller/btn_haut 0 0
force -freeze sim:/boutons_controller/btn_bas 0 0
force -freeze sim:/boutons_controller/btn_gauche 0 0
force -freeze sim:/boutons_controller/btn_droite 0 0
force -freeze sim:/boutons_controller/btn_centre 0 0
run 15625 ns
#force -freeze sim:/boutons_controller/clk 1 0, 0 {7812500 ps} -r {15625 ns}
force -freeze sim:/boutons_controller/rst 0 0
force -freeze sim:/boutons_controller/etat_systeme 1 0
force -freeze sim:/boutons_controller/btn_haut 0 0
force -freeze sim:/boutons_controller/btn_bas 0 0
force -freeze sim:/boutons_controller/btn_gauche 0 0
force -freeze sim:/boutons_controller/btn_droite 0 0
force -freeze sim:/boutons_controller/btn_centre 0 0
run 15625 ns

force -freeze sim:/boutons_controller/btn_haut 0 0
force -freeze sim:/boutons_controller/btn_bas 0 0
force -freeze sim:/boutons_controller/btn_gauche 0 0
force -freeze sim:/boutons_controller/btn_droite 1 0
force -freeze sim:/boutons_controller/btn_centre 0 0
run 10 ms


force -freeze sim:/boutons_controller/btn_gauche 1 0
run 10 ms