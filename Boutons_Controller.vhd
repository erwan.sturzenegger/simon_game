----------------------------------------------------------------------------------
-- Company: HEIA-FR
-- Engineer: Naël Telfser
-- 
-- Create Date:    17:59:19 05/17/2021 
-- Design Name:    TN2 Projet integré - Groupe 5
-- Module Name:    Boutons_Controller - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Boutons_Controller is
    Port ( clk : in  STD_LOGIC;
           rst : in  STD_LOGIC;
           btn_haut : in  STD_LOGIC;
           btn_bas : in  STD_LOGIC;
           btn_gauche : in  STD_LOGIC;
           btn_droite : in  STD_LOGIC;
           btn_centre : in  STD_LOGIC;
           btn_presse : out  STD_LOGIC_VECTOR (5 downto 0);
           btn_relache : out  STD_LOGIC_VECTOR (5 downto 0));
end Boutons_Controller;

architecture Behavioral of Boutons_Controller is
  signal btn_presse_present, btn_presse_futur : STD_LOGIC_VECTOR(5 downto 0);
  signal btn_relache_present, btn_relache_futur : STD_LOGIC_VECTOR(5 downto 0);
begin

  registre: process(clk, rst)
  begin
    if rst = '1' then
      btn_presse_present <= (others => '0');
      btn_relache_present <= (others => '0');
    elsif rising_edge(clk) then
      btn_presse_present <= btn_presse_futur;
      btn_relache_present <= btn_relache_futur;
    end if ;
  end process registre;

  combi_btn_presse: process(btn_haut, btn_bas, btn_gauche, btn_droite, btn_centre)
  begin
  
	  
	  if btn_droite = '1' and btn_gauche = '1' then
		btn_presse_futur <= "011100";
	  else
		  btn_presse_futur(5) <= (not btn_haut and not btn_bas and not btn_gauche and btn_centre) or (not btn_haut and not btn_bas and not btn_gauche and btn_droite);
		  btn_presse_futur(4) <= (not btn_haut and not btn_bas and btn_gauche and not btn_droite) or (not btn_haut and not btn_bas and btn_gauche and not btn_centre) or (not btn_haut and btn_bas and not btn_gauche and not btn_droite) or (not btn_haut and btn_bas and not btn_gauche and not btn_centre) or (not btn_haut and btn_gauche and not btn_droite and not btn_centre);
		  btn_presse_futur(3) <= (not btn_haut and not btn_bas and btn_gauche and not btn_centre) or (btn_haut and not btn_bas and not btn_gauche and not btn_centre) or (btn_haut and not btn_gauche and not btn_droite and not btn_centre) or (not btn_haut and not btn_bas and not btn_droite and btn_centre) or (not btn_bas and not btn_gauche and not btn_droite and btn_centre) or (not btn_bas and btn_gauche and not btn_droite and not btn_centre);
		  btn_presse_futur(2) <= (not btn_haut and not btn_bas and not btn_gauche and btn_droite and btn_centre) or (not btn_haut and not btn_bas and btn_gauche and not btn_droite and btn_centre) or (not btn_haut and btn_bas and not btn_gauche and not btn_droite and btn_centre) or (not btn_haut and btn_bas and not btn_gauche and btn_droite and not btn_centre) or (btn_haut and not btn_bas and not btn_gauche and not btn_droite and btn_centre) or (btn_haut and not btn_bas and not btn_gauche and btn_droite and not btn_centre);
		  btn_presse_futur(1) <= (not btn_haut and btn_bas and btn_gauche and not btn_droite and not btn_centre) or (btn_haut and not btn_bas and btn_gauche and not btn_droite and not btn_centre) or (btn_haut and btn_bas and not btn_gauche and not btn_droite and not btn_centre);
		  btn_presse_futur(0) <= (not btn_haut and not btn_bas and not btn_gauche and btn_droite and btn_centre) or (not btn_haut and not btn_bas and btn_gauche and not btn_droite and btn_centre) or (not btn_haut and btn_bas and not btn_gauche and not btn_droite and btn_centre) or (not btn_haut and btn_bas and btn_gauche and not btn_droite and not btn_centre) or (btn_haut and not btn_bas and not btn_gauche and not btn_droite and btn_centre) or (btn_haut and not btn_bas and btn_gauche and not btn_droite and not btn_centre);
	  end if;
  end process;

  combi_btn_relache: process(btn_presse_present, btn_droite, btn_gauche, btn_haut, btn_bas, btn_centre)
    variable btn_presse_present_5downto3 : STD_LOGIC_VECTOR(2 downto 0);
    variable btn_presse_present_2downto0 : STD_LOGIC_VECTOR(2 downto 0);
  begin
  
      btn_presse_present_5downto3 := btn_presse_present(5 downto 3);
      btn_presse_present_2downto0 := btn_presse_present(2 downto 0);

      if (btn_presse_present = "000000" or btn_presse_present_2downto0 = "000") AND (btn_haut = '1' OR btn_bas = '1' OR btn_gauche = '1' OR btn_droite = '1' OR btn_centre = '1') THEN
        btn_relache_futur <= "000000";
      else

      btn_relache_futur <= "000000";
      case( btn_presse_present_5downto3 ) is
        when "001" =>
          if btn_haut = '0' then
            btn_relache_futur(5 downto 3) <= btn_presse_present_5downto3;
          end if ;
        when "010" =>
          if btn_bas = '0' then
            btn_relache_futur(5 downto 3) <= btn_presse_present_5downto3;
          end if ;
        when "011" =>
          if btn_gauche = '0' then
            btn_relache_futur(5 downto 3) <= btn_presse_present_5downto3;
          end if ;
        when "100" =>
          if btn_droite = '0' then
            btn_relache_futur(5 downto 3) <= btn_presse_present_5downto3;
          end if ;
        when "101" =>
            if btn_centre = '0' then
            btn_relache_futur(5 downto 3) <= btn_presse_present_5downto3;
          end if ;
        when others =>
          btn_relache_futur(5 downto 3) <= btn_relache_present(5 downto 3);
      end case ;

      case( btn_presse_present_2downto0 ) is
        when "001" =>
          if btn_haut = '0' then
            btn_relache_futur(2 downto 0) <= btn_presse_present_2downto0;
          end if ;
        when "010" =>
          if btn_bas = '0' then
            btn_relache_futur(2 downto 0) <= btn_presse_present_2downto0;
          end if ;
        when "011" =>
          if btn_gauche = '0' then
            btn_relache_futur(2 downto 0) <= btn_presse_present_2downto0;
          end if ;
        when "100" =>
          if btn_droite = '0' then
            btn_relache_futur(2 downto 0) <= btn_presse_present_2downto0;
          end if ;
        when "101" =>
            if btn_centre = '0' then
            btn_relache_futur(2 downto 0) <= btn_presse_present_2downto0;
          end if ;
        when others =>
          btn_relache_futur(2 downto 0) <= btn_relache_present(2 downto 0);
      end case ;
      end if;

  end process;

  

  btn_presse <= btn_presse_present;
  btn_relache <= btn_relache_present;
end Behavioral;























