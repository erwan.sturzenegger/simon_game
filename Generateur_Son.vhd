----------------------------------------------------------------------------------
-- Company: HEIA-FR
-- Engineer: Naël Telfser
-- 
-- Create Date:    13:40:34 05/19/2021 
-- Design Name:    Simon_Game
-- Module Name:    Generateur_Son - Behavioral 
-- Project Name:   TN2 - Projet Intégré - Groupe 5
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Generateur_Son is
    Port ( clk : in  STD_LOGIC;
           rst : in  STD_LOGIC;
           etat_jeu : in  STD_LOGIC_VECTOR (2 downto 0);
           btn_presse : in  STD_LOGIC_VECTOR (5 downto 0);
           symbole : in  STD_LOGIC_VECTOR (2 downto 0);
           signal_2hz : in  STD_LOGIC;
           signal_3_3hz : in  STD_LOGIC;
           signal_209hz : in  STD_LOGIC;
           signal_252hz : in  STD_LOGIC;
           signal_310hz : in  STD_LOGIC;
           signal_370hz : in  STD_LOGIC;
           signal_415hz : in  STD_LOGIC;
           signal_524hz : in  STD_LOGIC;
           signal_441hz : in  STD_LOGIC;
           signal_262hz : in  STD_LOGIC;
           son : out  STD_LOGIC;
           etat_son : out  STD_LOGIC_VECTOR (3 downto 0);
           start_2hz : out  STD_LOGIC);
end Generateur_Son;

architecture Behavioral of Generateur_Son is
  type etats is (stop_son, clk_3_3hz, clk_209hz, clk_252hz, clk_310hz, clk_370hz, clk_415hz, clk_524hz, clk_441hz, clk_262hz);
  signal etat_son_present, etat_son_futur : etats;

  type etats_gagne_perdu is (arret, clk_524hz, clk_441hz, clk_262hz);

  signal etat_gagne_present, etat_gagne_futur: etats_gagne_perdu;
  signal etat_perdu_present, etat_perdu_futur: etats_gagne_perdu;

  signal start_2hz_present, start_2hz_futur: std_logic;

  signal signal_2hz_present, signal_2hz_futur: std_logic;

  signal signal_3_3hz_present, signal_3_3hz_futur: std_logic;
begin
  
  registre: process(clk, rst)
  begin
    if rst = '1' then
      etat_son_present <= stop_son;
      start_2hz_present <= '0';
    elsif rising_edge(clk) then
      etat_son_present <= etat_son_futur;
      start_2hz_present <= start_2hz_futur;
    end if ;
  end process registre;

  registre_gagne_perdu: process(clk, rst)
  begin
    if rst = '1' then
      etat_gagne_present <= arret;
      etat_perdu_present <= arret;
    elsif rising_edge(clk) then
      etat_gagne_present <= etat_gagne_futur;
      etat_perdu_present <= etat_perdu_futur;
    end if ;
  end process;

  combi_etat_gagne: process(etat_jeu, symbole, etat_son_present, etat_gagne_present)
  begin
    if etat_jeu = "011" then
      if symbole = "110" then
        case( etat_son_present ) is
          when clk_524hz =>
            etat_gagne_futur <= clk_524hz;
          when clk_441hz =>
            etat_gagne_futur <= clk_441hz;
          when clk_262hz =>
            etat_gagne_futur <= clk_262hz;
          when clk_3_3hz =>
            etat_gagne_futur <= etat_gagne_present;
          when others =>
            etat_gagne_futur <= arret;
        end case ;
      else
        etat_gagne_futur <= arret;
      end if ;
    else 
      etat_gagne_futur <= arret;
    end if ;
  end process;

  combi_etat_perdu: process(etat_jeu, symbole, etat_son_present, etat_perdu_present)
  begin
    if etat_jeu = "100" then
      if symbole = "111" then
        case( etat_son_present ) is
          when clk_262hz =>
            etat_perdu_futur <= clk_262hz;
          when clk_441hz =>
            etat_perdu_futur <= clk_441hz;
          when clk_524hz =>
            etat_perdu_futur <= clk_524hz;
          when clk_3_3hz =>
            etat_perdu_futur <= etat_perdu_present;
          when others =>
            etat_perdu_futur <= arret;
        end case ;
      else
        etat_perdu_futur <= arret;
      end if ;
    else 
      etat_perdu_futur <= arret;
    end if ;
  end process;

  registre_start_2hz: process(clk, rst)
  begin
    if rst = '1' then
      start_2hz_present <= '0';
    elsif rising_edge(clk) then
      start_2hz_present <= start_2hz_futur;
    end if ;
  end process;

  combi_start_2hz: process(etat_son_present, etat_jeu, symbole)
  begin
    if etat_jeu = "011" then
      if symbole = "110" then
        if etat_son_present = clk_524hz then
          start_2hz_futur <= '1';
        elsif etat_son_present = clk_3_3hz then
          start_2hz_futur <= '0';
        elsif etat_son_present = clk_441hz then
          start_2hz_futur <= '1';
        elsif etat_son_present = clk_262hz then
          start_2hz_futur <= '1';
        else
          start_2hz_futur <= '0';
        end if ;
      else
        start_2hz_futur <= '0';
      end if ;
    elsif etat_jeu = "100" then
      if symbole = "111" then
        if etat_son_present = clk_262hz then
          start_2hz_futur <= '1';
        elsif etat_son_present = clk_3_3hz then
          start_2hz_futur <= '0';
        elsif etat_son_present = clk_441hz then
          start_2hz_futur <= '1';
        elsif etat_son_present = clk_524hz then
          start_2hz_futur <= '1';
        else
          start_2hz_futur <= '0';
        end if ;
      else
        start_2hz_futur <= '0';
      end if ;
    else
      start_2hz_futur <= '0';
    end if ;
  end process;

  combi_etat_son: process(btn_presse, symbole, etat_jeu, etat_son_present, signal_2hz_futur, signal_2hz_present, etat_gagne_present, etat_perdu_present, signal_3_3hz_present, signal_3_3hz_futur)
  begin
    case( etat_jeu ) is
      when "001" =>
        case( symbole ) is
          when "001" =>
            etat_son_futur <= clk_310hz;
          when "010" =>
            etat_son_futur <= clk_252hz;
          when "011" =>
            etat_son_futur <= clk_370hz;
          when "100" =>
            etat_son_futur <= clk_415hz;
          when "101" =>
            etat_son_futur <= clk_209hz;
          when others =>
            etat_son_futur <= stop_son;
        end case ;
      when "010" =>
        if btn_presse = "001000" then
          etat_son_futur <= clk_310hz;
        elsif btn_presse = "010000" then
          etat_son_futur <= clk_252hz;
        elsif btn_presse = "011000" then
          etat_son_futur <= clk_370hz;
        elsif btn_presse = "100000" then
          etat_son_futur <= clk_415hz;
        elsif btn_presse = "101000" then
          etat_son_futur <= clk_209hz;
        elsif btn_presse = "000000" then
          etat_son_futur <= stop_son;
        else
          etat_son_futur <= stop_son;
        end if;
      when "011" =>
        case( symbole ) is
          when "110" =>
            if etat_son_present = stop_son then
              etat_son_futur <= clk_524hz;
            elsif etat_son_present = clk_524hz then
              if signal_2hz_present = '1' and signal_2hz_futur = '0' then
                etat_son_futur <= clk_3_3hz;
              else
                etat_son_futur <= clk_524hz;
              end if;
            elsif etat_son_present = clk_3_3hz then
              if signal_3_3hz_present = '1' and signal_3_3hz_futur = '0' then
                case( etat_gagne_present ) is
                  when clk_524hz =>
                    etat_son_futur <= clk_441hz;
                  when clk_441hz =>
                    etat_son_futur <= clk_262hz;
                  when others =>
                    etat_son_futur <= stop_son;
                end case ;
              else
                etat_son_futur <= clk_3_3hz;
              end if;
            elsif etat_son_present = clk_441hz then
              if signal_2hz_present = '1' and signal_2hz_futur = '0' then
                etat_son_futur <= clk_3_3hz;
              else
                etat_son_futur <= clk_441hz;
              end if;
            elsif etat_son_present = clk_262hz then
              if signal_2hz_present = '1' and signal_2hz_futur = '0' then
                etat_son_futur <= stop_son;
              else
                etat_son_futur <= clk_262hz;
              end if;
            else
              etat_son_futur <= stop_son;
            end if ;
          when others =>
            etat_son_futur <= stop_son;
        end case ;
      when "100" =>
        case( symbole ) is
          when "111" =>
            if etat_son_present = stop_son then
              etat_son_futur <= clk_262hz;
            elsif etat_son_present = clk_262hz then
              if signal_2hz_present = '1' and signal_2hz_futur = '0' then
                etat_son_futur <= clk_3_3hz;
              else
                etat_son_futur <= clk_262hz;
              end if;
            elsif etat_son_present = clk_3_3hz then
              if signal_3_3hz_present = '1' and signal_3_3hz_futur = '0' then
                case( etat_perdu_present ) is
                  when clk_262hz =>
                    etat_son_futur <= clk_441hz;
                  when clk_441hz =>
                    etat_son_futur <= clk_524hz;
                  when others =>
                    etat_son_futur <= stop_son;
                end case ;
              else
                etat_son_futur <= clk_3_3hz;
              end if;
            elsif etat_son_present = clk_441hz then
              if signal_2hz_present = '1' and signal_2hz_futur = '0' then
                etat_son_futur <= clk_3_3hz;
              else
                etat_son_futur <= clk_441hz;
              end if;
            elsif etat_son_present = clk_524hz then
              if signal_2hz_present = '1' and signal_2hz_futur = '0' then
                etat_son_futur <= stop_son;
              else
                etat_son_futur <= clk_524hz;
              end if;
            else
              etat_son_futur <= stop_son;
            end if ;
          when others =>
            etat_son_futur <= stop_son;
        end case ;
      when others =>
        etat_son_futur <= stop_son;
    end case ;
  end process;

  registre_signal_2hz:process(clk, rst)
  begin
    if(rst='1') then
      signal_2hz_present <= '0';
    elsif(rising_edge(clk)) then
      signal_2hz_present <= signal_2hz_futur;
    end if;
  end process;


  combi_signal_2hz:process(signal_2hz)
  begin
    signal_2hz_futur <= signal_2hz;
  end process;

  registre_signal_3_3hz: process(clk, rst)
  begin
    if rst = '1' then
      signal_3_3hz_present <= '0';
    elsif rising_edge(clk) then
      signal_3_3hz_present <= signal_3_3hz_futur;
    end if ;
  end process;

  combi_signal_3_3hz: process(signal_3_3hz)
  begin
    signal_3_3hz_futur <= signal_3_3hz;
  end process;

  etat_son <= "0000" when etat_son_present = stop_son else
              "0001" when etat_son_present = clk_3_3hz else
              "0010" when etat_son_present = clk_209hz else
              "0011" when etat_son_present = clk_252hz else
              "0100" when etat_son_present = clk_310hz else
              "0101" when etat_son_present = clk_370hz else
              "0110" when etat_son_present = clk_415hz else
              "0111" when etat_son_present = clk_524hz else
              "1000" when etat_son_present = clk_441hz else
              "1001" when etat_son_present = clk_262hz else
              "0000"; 

  start_2hz <= start_2hz_present;

  son <= '1' when signal_209hz = '1' or signal_252hz = '1' or signal_310hz = '1' or signal_370hz = '1' or signal_415hz = '1' or signal_524hz = '1' or signal_441hz = '1' or signal_262hz = '1' else '0';
end Behavioral;