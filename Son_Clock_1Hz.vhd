----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    11:53:02 05/19/2021 
-- Design Name: 
-- Module Name:    Son_Clock_1Hz - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Son_Clock_1Hz is
    Port ( clk : in  STD_LOGIC;
           rst : in  STD_LOGIC;
           start_1hz : in  STD_LOGIC;
           signal_1hz : out  STD_LOGIC);
end Son_Clock_1Hz;

architecture Behavioral of Son_Clock_1Hz is
	type etats is (bas, haut);

	signal etat_present_1hz, etat_futur_1hz : etats;
	signal compteur_present_1hz, compteur_futur_1hz : Unsigned(15 downto 0) := (others => '0');
	constant demi_second : Unsigned(15 downto 0) := to_unsigned(64000, 16);
	signal compte_1hz, fin_comptage_1hz : std_logic;

begin
	compte_1hz <= '1' when start_1hz = '1' else '0';
	fin_comptage_1hz <= '1' when compteur_present_1hz = demi_second else '0';

	combi_1hz : process(etat_present_1hz, compte_1hz, fin_comptage_1hz)
	begin
		case( etat_present_1hz ) is
		
			when bas =>
				if fin_comptage_1hz = '1' then
					etat_futur_1hz <= haut;
				else
					etat_futur_1hz <= bas;
				end if ;
			when haut =>
				if compte_1hz = '0' or fin_comptage_1hz = '1' then
					etat_futur_1hz <= bas;
				else
					etat_futur_1hz <= haut;
				end if ;
			when others => etat_futur_1hz <= bas;
		end case ;
	end process;

	registre_1hz: process(clk, rst)
	begin
		if rst = '1' then
			etat_present_1hz <= bas;
		elsif rising_edge(clk) then
			etat_present_1hz <= etat_futur_1hz;
		end if ;
	end process;

	registre_compteur_1hz: process(clk, rst)
	begin
		if rst = '1' then
			compteur_present_1hz <= (others => '0');
		elsif rising_edge(clk) then
			compteur_present_1hz <= compteur_futur_1hz;
		end if ;
	end process;

	combi_compteur_1hz: process(compteur_present_1hz, compte_1hz, fin_comptage_1hz)
	begin

		if compte_1hz = '0' or fin_comptage_1hz = '1' then
			compteur_futur_1hz <= (others => '0');
		else
			compteur_futur_1hz <= compteur_present_1hz + 1;
		end if ;
	end process;
	signal_1hz <= '1' when etat_present_1hz = haut else '0';
end Behavioral;

