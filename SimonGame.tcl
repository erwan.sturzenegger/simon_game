 do {SimonGame.fdo}

force -freeze sim:/simongame/clk 1 0, 0 {7812500 ps} -r {15625 ns}
force -freeze sim:/simongame/rst 1 0
force -freeze sim:/simongame/btn_gauche 0 0
force -freeze sim:/simongame/btn_droite 0 0
force -freeze sim:/simongame/btn_centre 0 0
force -freeze sim:/simongame/btn_haut 0 0
force -freeze sim:/simongame/btn_bas 0 0
run 10 ms

force -freeze sim:/simongame/rst 0 0
force -freeze sim:/simongame/btn_gauche 1 0
force -freeze sim:/simongame/btn_droite 1 0
run 1100 ms

force -freeze sim:/simongame/btn_gauche 0 0
force -freeze sim:/simongame/btn_droite 0 0
run 2000 ms


#La séquence est : 001 010 011 101 010 011 001 100 001 001 100 101 010 100 011 100
## => droite -> gauche -> droite -> bas -> centre -> droite -> 

####### SEQUENCE NO 1 ####### => Droite | 100

force -freeze sim:/simongame/btn_gauche 0 0
force -freeze sim:/simongame/btn_droite 1 0
force -freeze sim:/simongame/btn_centre 0 0
force -freeze sim:/simongame/btn_haut 0 0
force -freeze sim:/simongame/btn_bas 0 0
run 200 ms 
force -freeze sim:/simongame/btn_droite 0 0
run 4100 ms

####### SEQUENCE NO 2 ####### => Gauche | 011

force -freeze sim:/simongame/btn_gauche 0 0
force -freeze sim:/simongame/btn_droite 1 0
force -freeze sim:/simongame/btn_centre 0 0
force -freeze sim:/simongame/btn_haut 0 0
force -freeze sim:/simongame/btn_bas 0 0
run 200 ms 
force -freeze sim:/simongame/btn_droite 0 0
run 200 ms
force -freeze sim:/simongame/btn_gauche 1 0
force -freeze sim:/simongame/btn_droite 0 0
force -freeze sim:/simongame/btn_centre 0 0
force -freeze sim:/simongame/btn_haut 0 0
force -freeze sim:/simongame/btn_bas 0 0
run 200 ms 
force -freeze sim:/simongame/btn_gauche 0 0
run 6100 ms

####### SEQUENCE NO 3 ####### => Droite | 100

force -freeze sim:/simongame/btn_gauche 0 0
force -freeze sim:/simongame/btn_droite 1 0
force -freeze sim:/simongame/btn_centre 0 0
force -freeze sim:/simongame/btn_haut 0 0
force -freeze sim:/simongame/btn_bas 0 0
run 200 ms 
force -freeze sim:/simongame/btn_droite 0 0
run 200 ms
force -freeze sim:/simongame/btn_gauche 1 0
force -freeze sim:/simongame/btn_droite 0 0
force -freeze sim:/simongame/btn_centre 0 0
force -freeze sim:/simongame/btn_haut 0 0
force -freeze sim:/simongame/btn_bas 0 0
run 200 ms 
force -freeze sim:/simongame/btn_gauche 0 0
run 200 ms
force -freeze sim:/simongame/btn_gauche 0 0
force -freeze sim:/simongame/btn_droite 1 0
force -freeze sim:/simongame/btn_centre 0 0
force -freeze sim:/simongame/btn_haut 0 0
force -freeze sim:/simongame/btn_bas 0 0
run 200 ms 
force -freeze sim:/simongame/btn_droite 0 0
run 8100 ms

####### SEQUENCE NO 5 #######

force -freeze sim:/simongame/btn_gauche 0 0
force -freeze sim:/simongame/btn_droite 1 0
force -freeze sim:/simongame/btn_centre 0 0
force -freeze sim:/simongame/btn_haut 0 0
force -freeze sim:/simongame/btn_bas 0 0
run 200 ms 
force -freeze sim:/simongame/btn_droite 0 0
run 200 ms
force -freeze sim:/simongame/btn_gauche 1 0
force -freeze sim:/simongame/btn_droite 0 0
force -freeze sim:/simongame/btn_centre 0 0
force -freeze sim:/simongame/btn_haut 0 0
force -freeze sim:/simongame/btn_bas 0 0
run 200 ms 
force -freeze sim:/simongame/btn_gauche 0 0
run 200 ms
force -freeze sim:/simongame/btn_gauche 0 0
force -freeze sim:/simongame/btn_droite 1 0
force -freeze sim:/simongame/btn_centre 0 0
force -freeze sim:/simongame/btn_haut 0 0
force -freeze sim:/simongame/btn_bas 0 0
run 200 ms 
force -freeze sim:/simongame/btn_droite 0 0
run 200 ms
force -freeze sim:/simongame/btn_gauche 0 0
force -freeze sim:/simongame/btn_droite 0 0
force -freeze sim:/simongame/btn_centre 0 0
force -freeze sim:/simongame/btn_haut 0 0
force -freeze sim:/simongame/btn_bas 1 0
run 200 ms 
force -freeze sim:/simongame/btn_bas 0 0
run 10100 ms

####### SEQUENCE NO 6 #######

force -freeze sim:/simongame/btn_gauche 0 0
force -freeze sim:/simongame/btn_droite 1 0
force -freeze sim:/simongame/btn_centre 0 0
force -freeze sim:/simongame/btn_haut 0 0
force -freeze sim:/simongame/btn_bas 0 0
run 200 ms 
force -freeze sim:/simongame/btn_droite 0 0
run 200 ms
force -freeze sim:/simongame/btn_gauche 1 0
force -freeze sim:/simongame/btn_droite 0 0
force -freeze sim:/simongame/btn_centre 0 0
force -freeze sim:/simongame/btn_haut 0 0
force -freeze sim:/simongame/btn_bas 0 0
run 200 ms 
force -freeze sim:/simongame/btn_gauche 0 0
run 200 ms
force -freeze sim:/simongame/btn_gauche 0 0
force -freeze sim:/simongame/btn_droite 1 0
force -freeze sim:/simongame/btn_centre 0 0
force -freeze sim:/simongame/btn_haut 0 0
force -freeze sim:/simongame/btn_bas 0 0
run 200 ms 
force -freeze sim:/simongame/btn_droite 0 0
run 200 ms
force -freeze sim:/simongame/btn_gauche 0 0
force -freeze sim:/simongame/btn_droite 0 0
force -freeze sim:/simongame/btn_centre 0 0
force -freeze sim:/simongame/btn_haut 0 0
force -freeze sim:/simongame/btn_bas 1 0
run 200 ms 
force -freeze sim:/simongame/btn_bas 0 0
run 10100 ms

####### SEQUENCE NO 7 #######

force -freeze sim:/simongame/btn_gauche 0 0
force -freeze sim:/simongame/btn_droite 1 0
force -freeze sim:/simongame/btn_centre 0 0
force -freeze sim:/simongame/btn_haut 0 0
force -freeze sim:/simongame/btn_bas 0 0
run 200 ms 
force -freeze sim:/simongame/btn_droite 0 0
run 200 ms
force -freeze sim:/simongame/btn_gauche 1 0
force -freeze sim:/simongame/btn_droite 0 0
force -freeze sim:/simongame/btn_centre 0 0
force -freeze sim:/simongame/btn_haut 0 0
force -freeze sim:/simongame/btn_bas 0 0
run 200 ms 
force -freeze sim:/simongame/btn_gauche 0 0
run 200 ms
force -freeze sim:/simongame/btn_gauche 0 0
force -freeze sim:/simongame/btn_droite 1 0
force -freeze sim:/simongame/btn_centre 0 0
force -freeze sim:/simongame/btn_haut 0 0
force -freeze sim:/simongame/btn_bas 0 0
run 200 ms 
force -freeze sim:/simongame/btn_droite 0 0
run 200 ms
force -freeze sim:/simongame/btn_gauche 0 0
force -freeze sim:/simongame/btn_droite 0 0
force -freeze sim:/simongame/btn_centre 0 0
force -freeze sim:/simongame/btn_haut 0 0
force -freeze sim:/simongame/btn_bas 1 0
run 200 ms 
force -freeze sim:/simongame/btn_bas 0 0
run 10100 ms

####### SEQUENCE NO 8 #######

force -freeze sim:/simongame/btn_gauche 0 0
force -freeze sim:/simongame/btn_droite 1 0
force -freeze sim:/simongame/btn_centre 0 0
force -freeze sim:/simongame/btn_haut 0 0
force -freeze sim:/simongame/btn_bas 0 0
run 200 ms 
force -freeze sim:/simongame/btn_droite 0 0
run 200 ms
force -freeze sim:/simongame/btn_gauche 1 0
force -freeze sim:/simongame/btn_droite 0 0
force -freeze sim:/simongame/btn_centre 0 0
force -freeze sim:/simongame/btn_haut 0 0
force -freeze sim:/simongame/btn_bas 0 0
run 200 ms 
force -freeze sim:/simongame/btn_gauche 0 0
run 200 ms
force -freeze sim:/simongame/btn_gauche 0 0
force -freeze sim:/simongame/btn_droite 1 0
force -freeze sim:/simongame/btn_centre 0 0
force -freeze sim:/simongame/btn_haut 0 0
force -freeze sim:/simongame/btn_bas 0 0
run 200 ms 
force -freeze sim:/simongame/btn_droite 0 0
run 200 ms
force -freeze sim:/simongame/btn_gauche 0 0
force -freeze sim:/simongame/btn_droite 0 0
force -freeze sim:/simongame/btn_centre 0 0
force -freeze sim:/simongame/btn_haut 0 0
force -freeze sim:/simongame/btn_bas 1 0
run 200 ms 
force -freeze sim:/simongame/btn_bas 0 0
run 10100 ms

####### SEQUENCE NO 9 #######

force -freeze sim:/simongame/btn_gauche 0 0
force -freeze sim:/simongame/btn_droite 1 0
force -freeze sim:/simongame/btn_centre 0 0
force -freeze sim:/simongame/btn_haut 0 0
force -freeze sim:/simongame/btn_bas 0 0
run 200 ms 
force -freeze sim:/simongame/btn_droite 0 0
run 200 ms
force -freeze sim:/simongame/btn_gauche 1 0
force -freeze sim:/simongame/btn_droite 0 0
force -freeze sim:/simongame/btn_centre 0 0
force -freeze sim:/simongame/btn_haut 0 0
force -freeze sim:/simongame/btn_bas 0 0
run 200 ms 
force -freeze sim:/simongame/btn_gauche 0 0
run 200 ms
force -freeze sim:/simongame/btn_gauche 0 0
force -freeze sim:/simongame/btn_droite 1 0
force -freeze sim:/simongame/btn_centre 0 0
force -freeze sim:/simongame/btn_haut 0 0
force -freeze sim:/simongame/btn_bas 0 0
run 200 ms 
force -freeze sim:/simongame/btn_droite 0 0
run 200 ms
force -freeze sim:/simongame/btn_gauche 0 0
force -freeze sim:/simongame/btn_droite 0 0
force -freeze sim:/simongame/btn_centre 0 0
force -freeze sim:/simongame/btn_haut 0 0
force -freeze sim:/simongame/btn_bas 1 0
run 200 ms 
force -freeze sim:/simongame/btn_bas 0 0
run 10100 ms

####### SEQUENCE NO 10 #######

force -freeze sim:/simongame/btn_gauche 0 0
force -freeze sim:/simongame/btn_droite 1 0
force -freeze sim:/simongame/btn_centre 0 0
force -freeze sim:/simongame/btn_haut 0 0
force -freeze sim:/simongame/btn_bas 0 0
run 200 ms 
force -freeze sim:/simongame/btn_droite 0 0
run 200 ms
force -freeze sim:/simongame/btn_gauche 1 0
force -freeze sim:/simongame/btn_droite 0 0
force -freeze sim:/simongame/btn_centre 0 0
force -freeze sim:/simongame/btn_haut 0 0
force -freeze sim:/simongame/btn_bas 0 0
run 200 ms 
force -freeze sim:/simongame/btn_gauche 0 0
run 200 ms
force -freeze sim:/simongame/btn_gauche 0 0
force -freeze sim:/simongame/btn_droite 1 0
force -freeze sim:/simongame/btn_centre 0 0
force -freeze sim:/simongame/btn_haut 0 0
force -freeze sim:/simongame/btn_bas 0 0
run 200 ms 
force -freeze sim:/simongame/btn_droite 0 0
run 200 ms
force -freeze sim:/simongame/btn_gauche 0 0
force -freeze sim:/simongame/btn_droite 0 0
force -freeze sim:/simongame/btn_centre 0 0
force -freeze sim:/simongame/btn_haut 0 0
force -freeze sim:/simongame/btn_bas 1 0
run 200 ms 
force -freeze sim:/simongame/btn_bas 0 0
run 10100 ms




