force -freeze sim:/son_clock_252hz/clk 1 0, 0 {7812500 ps} -r {15625 ns}
# Test état 252hz
force -freeze sim:/son_clock_252hz/rst 0 0
force -freeze sim:/son_clock_252hz/etat_son 0011 0
run 1000 ms
# Test reset
force -freeze sim:/son_clock_252hz/rst 1 0
force -freeze sim:/son_clock_252hz/etat_son 0000 0
run 1000 ms
# Test état 252hz
force -freeze sim:/son_clock_252hz/rst 0 0
force -freeze sim:/son_clock_252hz/etat_son 0011 0
run 1000 ms
# Test autre état
force -freeze sim:/son_clock_252hz/rst 0 0
force -freeze sim:/son_clock_252hz/etat_son 1000 0
run 1000 ms