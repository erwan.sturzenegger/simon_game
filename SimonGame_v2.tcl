do {SimonGame.fdo}

force -freeze sim:/simongame/clk 1 0, 0 {7812500 ps} -r {15625 ns}
force -freeze sim:/simongame/rst 1 0
force -freeze sim:/simongame/btn_gauche 0 0
force -freeze sim:/simongame/btn_droite 0 0
force -freeze sim:/simongame/btn_centre 0 0
force -freeze sim:/simongame/btn_haut 0 0
force -freeze sim:/simongame/btn_bas 0 0
run 10 ms

force -freeze sim:/simongame/rst 0 0
force -freeze sim:/simongame/btn_gauche 1 0
force -freeze sim:/simongame/btn_droite 1 0
run 1100 ms

force -freeze sim:/simongame/btn_gauche 0 0
force -freeze sim:/simongame/btn_droite 0 0
run 2000 ms


#La s�quence est : 001 010 011 101 010 011 001 100 001 001 100 101 010 100 011 100

####### TEST AVEC SEQUENCE JUSTE #######

####### SEQUENCE 1 ########

force -freeze sim:/simongame/btn_droite 1 0
run 200 ms 
force -freeze sim:/simongame/btn_droite 0 0
run 4100 ms

####### SEQUENCE 2 ########

force -freeze sim:/simongame/btn_droite 1 0
run 200 ms 
force -freeze sim:/simongame/btn_droite 0 0
run 200 ms 
force -freeze sim:/simongame/btn_gauche 1 0
run 200 ms 
force -freeze sim:/simongame/btn_gauche 0 0
run 6100 ms

####### SEQUENCE 3 ########

force -freeze sim:/simongame/btn_droite 1 0
run 200 ms 
force -freeze sim:/simongame/btn_droite 0 0
run 200 ms 
force -freeze sim:/simongame/btn_gauche 1 0
run 200 ms 
force -freeze sim:/simongame/btn_gauche 0 0
run 200 ms 
force -freeze sim:/simongame/btn_droite 1 0
run 200 ms 
force -freeze sim:/simongame/btn_droite 0 0
run 8100 ms

####### SEQUENCE 4 ########

force -freeze sim:/simongame/btn_droite 1 0
run 200 ms 
force -freeze sim:/simongame/btn_droite 0 0
run 200 ms 
force -freeze sim:/simongame/btn_gauche 1 0
run 200 ms 
force -freeze sim:/simongame/btn_gauche 0 0
run 200 ms 
force -freeze sim:/simongame/btn_droite 1 0
run 200 ms 
force -freeze sim:/simongame/btn_droite 0 0
run 200 ms 
force -freeze sim:/simongame/btn_bas 1 0
run 200 ms 
force -freeze sim:/simongame/btn_bas 0 0
run 10100 ms

####### SEQUENCE 5 ########

force -freeze sim:/simongame/btn_droite 1 0
run 200 ms 
force -freeze sim:/simongame/btn_droite 0 0
run 200 ms 
force -freeze sim:/simongame/btn_gauche 1 0
run 200 ms 
force -freeze sim:/simongame/btn_gauche 0 0
run 200 ms 
force -freeze sim:/simongame/btn_droite 1 0
run 200 ms 
force -freeze sim:/simongame/btn_droite 0 0
run 200 ms 
force -freeze sim:/simongame/btn_bas 1 0
run 200 ms 
force -freeze sim:/simongame/btn_bas 0 0
run 200 ms 
force -freeze sim:/simongame/btn_centre 1 0
run 200 ms 
force -freeze sim:/simongame/btn_centre 0 0
run 12100 ms

####### SEQUENCE 6 ########

force -freeze sim:/simongame/btn_droite 1 0
run 200 ms 
force -freeze sim:/simongame/btn_droite 0 0
run 200 ms 
force -freeze sim:/simongame/btn_gauche 1 0
run 200 ms 
force -freeze sim:/simongame/btn_gauche 0 0
run 200 ms 
force -freeze sim:/simongame/btn_droite 1 0
run 200 ms 
force -freeze sim:/simongame/btn_droite 0 0
run 200 ms 
force -freeze sim:/simongame/btn_bas 1 0
run 200 ms 
force -freeze sim:/simongame/btn_bas 0 0
run 200 ms 
force -freeze sim:/simongame/btn_centre 1 0
run 200 ms 
force -freeze sim:/simongame/btn_centre 0 0
run 200 ms 
force -freeze sim:/simongame/btn_droite 1 0
run 200 ms 
force -freeze sim:/simongame/btn_droite 0 0
run 14100 ms

####### SEQUENCE 7 ########

force -freeze sim:/simongame/btn_droite 1 0
run 200 ms 
force -freeze sim:/simongame/btn_droite 0 0
run 200 ms 
force -freeze sim:/simongame/btn_gauche 1 0
run 200 ms 
force -freeze sim:/simongame/btn_gauche 0 0
run 200 ms 
force -freeze sim:/simongame/btn_droite 1 0
run 200 ms 
force -freeze sim:/simongame/btn_droite 0 0
run 200 ms 
force -freeze sim:/simongame/btn_bas 1 0
run 200 ms 
force -freeze sim:/simongame/btn_bas 0 0
run 200 ms 
force -freeze sim:/simongame/btn_centre 1 0
run 200 ms 
force -freeze sim:/simongame/btn_centre 0 0
run 200 ms 
force -freeze sim:/simongame/btn_droite 1 0
run 200 ms 
force -freeze sim:/simongame/btn_droite 0 0
run 200 ms 
force -freeze sim:/simongame/btn_haut 1 0
run 200 ms 
force -freeze sim:/simongame/btn_haut 0 0
run 16100 ms

####### SEQUENCE 8 ########

force -freeze sim:/simongame/btn_droite 1 0
run 200 ms 
force -freeze sim:/simongame/btn_droite 0 0
run 200 ms 
force -freeze sim:/simongame/btn_gauche 1 0
run 200 ms 
force -freeze sim:/simongame/btn_gauche 0 0
run 200 ms 
force -freeze sim:/simongame/btn_droite 1 0
run 200 ms 
force -freeze sim:/simongame/btn_droite 0 0
run 200 ms 
force -freeze sim:/simongame/btn_bas 1 0
run 200 ms 
force -freeze sim:/simongame/btn_bas 0 0
run 200 ms 
force -freeze sim:/simongame/btn_centre 1 0
run 200 ms 
force -freeze sim:/simongame/btn_centre 0 0
run 200 ms 
force -freeze sim:/simongame/btn_droite 1 0
run 200 ms 
force -freeze sim:/simongame/btn_droite 0 0
run 200 ms 
force -freeze sim:/simongame/btn_haut 1 0
run 200 ms 
force -freeze sim:/simongame/btn_haut 0 0
run 200 ms 
force -freeze sim:/simongame/btn_haut 1 0
run 200 ms 
force -freeze sim:/simongame/btn_haut 0 0
run 18100 ms

####### SEQUENCE 9 ########

force -freeze sim:/simongame/btn_droite 1 0
run 200 ms 
force -freeze sim:/simongame/btn_droite 0 0
run 200 ms 
force -freeze sim:/simongame/btn_gauche 1 0
run 200 ms 
force -freeze sim:/simongame/btn_gauche 0 0
run 200 ms 
force -freeze sim:/simongame/btn_droite 1 0
run 200 ms 
force -freeze sim:/simongame/btn_droite 0 0
run 200 ms 
force -freeze sim:/simongame/btn_bas 1 0
run 200 ms 
force -freeze sim:/simongame/btn_bas 0 0
run 200 ms 
force -freeze sim:/simongame/btn_centre 1 0
run 200 ms 
force -freeze sim:/simongame/btn_centre 0 0
run 200 ms 
force -freeze sim:/simongame/btn_droite 1 0
run 200 ms 
force -freeze sim:/simongame/btn_droite 0 0
run 200 ms 
force -freeze sim:/simongame/btn_haut 1 0
run 200 ms 
force -freeze sim:/simongame/btn_haut 0 0
run 200 ms 
force -freeze sim:/simongame/btn_haut 1 0
run 200 ms 
force -freeze sim:/simongame/btn_haut 0 0
run 200 ms 
force -freeze sim:/simongame/btn_droite 1 0
run 200 ms 
force -freeze sim:/simongame/btn_droite 0 0
run 20100 ms

####### SEQUENCE 10 ########

force -freeze sim:/simongame/btn_droite 1 0
run 200 ms 
force -freeze sim:/simongame/btn_droite 0 0
run 200 ms 
force -freeze sim:/simongame/btn_gauche 1 0
run 200 ms 
force -freeze sim:/simongame/btn_gauche 0 0
run 200 ms 
force -freeze sim:/simongame/btn_droite 1 0
run 200 ms 
force -freeze sim:/simongame/btn_droite 0 0
run 200 ms 
force -freeze sim:/simongame/btn_bas 1 0
run 200 ms 
force -freeze sim:/simongame/btn_bas 0 0
run 200 ms 
force -freeze sim:/simongame/btn_centre 1 0
run 200 ms 
force -freeze sim:/simongame/btn_centre 0 0
run 200 ms 
force -freeze sim:/simongame/btn_droite 1 0
run 200 ms 
force -freeze sim:/simongame/btn_droite 0 0
run 200 ms 
force -freeze sim:/simongame/btn_haut 1 0
run 200 ms 
force -freeze sim:/simongame/btn_haut 0 0
run 200 ms 
force -freeze sim:/simongame/btn_haut 1 0
run 200 ms 
force -freeze sim:/simongame/btn_haut 0 0
run 200 ms 
force -freeze sim:/simongame/btn_droite 1 0
run 200 ms 
force -freeze sim:/simongame/btn_droite 0 0
run 200 ms 
force -freeze sim:/simongame/btn_haut 1 0
run 200 ms 
force -freeze sim:/simongame/btn_haut 0 0
run 22100 ms

####### SEQUENCE 11 ########

force -freeze sim:/simongame/btn_droite 1 0
run 200 ms 
force -freeze sim:/simongame/btn_droite 0 0
run 200 ms 
force -freeze sim:/simongame/btn_gauche 1 0
run 200 ms 
force -freeze sim:/simongame/btn_gauche 0 0
run 200 ms 
force -freeze sim:/simongame/btn_droite 1 0
run 200 ms 
force -freeze sim:/simongame/btn_droite 0 0
run 200 ms 
force -freeze sim:/simongame/btn_bas 1 0
run 200 ms 
force -freeze sim:/simongame/btn_bas 0 0
run 200 ms 
force -freeze sim:/simongame/btn_centre 1 0
run 200 ms 
force -freeze sim:/simongame/btn_centre 0 0
run 200 ms 
force -freeze sim:/simongame/btn_droite 1 0
run 200 ms 
force -freeze sim:/simongame/btn_droite 0 0
run 200 ms 
force -freeze sim:/simongame/btn_haut 1 0
run 200 ms 
force -freeze sim:/simongame/btn_haut 0 0
run 200 ms 
force -freeze sim:/simongame/btn_haut 1 0
run 200 ms 
force -freeze sim:/simongame/btn_haut 0 0
run 200 ms 
force -freeze sim:/simongame/btn_droite 1 0
run 200 ms 
force -freeze sim:/simongame/btn_droite 0 0
run 200 ms 
force -freeze sim:/simongame/btn_haut 1 0
run 200 ms 
force -freeze sim:/simongame/btn_haut 0 0
run 200 ms 
force -freeze sim:/simongame/btn_gauche 1 0
run 200 ms 
force -freeze sim:/simongame/btn_gauche 0 0
run 24100 ms

####### SEQUENCE 12 ########

force -freeze sim:/simongame/btn_droite 1 0
run 200 ms 
force -freeze sim:/simongame/btn_droite 0 0
run 200 ms 
force -freeze sim:/simongame/btn_gauche 1 0
run 200 ms 
force -freeze sim:/simongame/btn_gauche 0 0
run 200 ms 
force -freeze sim:/simongame/btn_droite 1 0
run 200 ms 
force -freeze sim:/simongame/btn_droite 0 0
run 200 ms 
force -freeze sim:/simongame/btn_bas 1 0
run 200 ms 
force -freeze sim:/simongame/btn_bas 0 0
run 200 ms 
force -freeze sim:/simongame/btn_centre 1 0
run 200 ms 
force -freeze sim:/simongame/btn_centre 0 0
run 200 ms 
force -freeze sim:/simongame/btn_droite 1 0
run 200 ms 
force -freeze sim:/simongame/btn_droite 0 0
run 200 ms 
force -freeze sim:/simongame/btn_haut 1 0
run 200 ms 
force -freeze sim:/simongame/btn_haut 0 0
run 200 ms 
force -freeze sim:/simongame/btn_haut 1 0
run 200 ms 
force -freeze sim:/simongame/btn_haut 0 0
run 200 ms 
force -freeze sim:/simongame/btn_droite 1 0
run 200 ms 
force -freeze sim:/simongame/btn_droite 0 0
run 200 ms 
force -freeze sim:/simongame/btn_haut 1 0
run 200 ms 
force -freeze sim:/simongame/btn_haut 0 0
run 200 ms 
force -freeze sim:/simongame/btn_gauche 1 0
run 200 ms 
force -freeze sim:/simongame/btn_gauche 0 0
run 200 ms 
force -freeze sim:/simongame/btn_bas 1 0
run 200 ms 
force -freeze sim:/simongame/btn_bas 0 0
run 26100 ms

####### SEQUENCE 13 ########

force -freeze sim:/simongame/btn_droite 1 0
run 200 ms 
force -freeze sim:/simongame/btn_droite 0 0
run 200 ms 
force -freeze sim:/simongame/btn_gauche 1 0
run 200 ms 
force -freeze sim:/simongame/btn_gauche 0 0
run 200 ms 
force -freeze sim:/simongame/btn_droite 1 0
run 200 ms 
force -freeze sim:/simongame/btn_droite 0 0
run 200 ms 
force -freeze sim:/simongame/btn_bas 1 0
run 200 ms 
force -freeze sim:/simongame/btn_bas 0 0
run 200 ms 
force -freeze sim:/simongame/btn_centre 1 0
run 200 ms 
force -freeze sim:/simongame/btn_centre 0 0
run 200 ms 
force -freeze sim:/simongame/btn_droite 1 0
run 200 ms 
force -freeze sim:/simongame/btn_droite 0 0
run 200 ms 
force -freeze sim:/simongame/btn_haut 1 0
run 200 ms 
force -freeze sim:/simongame/btn_haut 0 0
run 200 ms 
force -freeze sim:/simongame/btn_haut 1 0
run 200 ms 
force -freeze sim:/simongame/btn_haut 0 0
run 200 ms 
force -freeze sim:/simongame/btn_droite 1 0
run 200 ms 
force -freeze sim:/simongame/btn_droite 0 0
run 200 ms 
force -freeze sim:/simongame/btn_haut 1 0
run 200 ms 
force -freeze sim:/simongame/btn_haut 0 0
run 200 ms 
force -freeze sim:/simongame/btn_gauche 1 0
run 200 ms 
force -freeze sim:/simongame/btn_gauche 0 0
run 200 ms 
force -freeze sim:/simongame/btn_bas 1 0
run 200 ms 
force -freeze sim:/simongame/btn_bas 0 0
run 200 ms 
force -freeze sim:/simongame/btn_centre 1 0
run 200 ms 
force -freeze sim:/simongame/btn_centre 0 0
run 28100 ms

####### SEQUENCE 14 ########

force -freeze sim:/simongame/btn_droite 1 0
run 200 ms 
force -freeze sim:/simongame/btn_droite 0 0
run 200 ms 
force -freeze sim:/simongame/btn_gauche 1 0
run 200 ms 
force -freeze sim:/simongame/btn_gauche 0 0
run 200 ms 
force -freeze sim:/simongame/btn_droite 1 0
run 200 ms 
force -freeze sim:/simongame/btn_droite 0 0
run 200 ms 
force -freeze sim:/simongame/btn_bas 1 0
run 200 ms 
force -freeze sim:/simongame/btn_bas 0 0
run 200 ms 
force -freeze sim:/simongame/btn_centre 1 0
run 200 ms 
force -freeze sim:/simongame/btn_centre 0 0
run 200 ms 
force -freeze sim:/simongame/btn_droite 1 0
run 200 ms 
force -freeze sim:/simongame/btn_droite 0 0
run 200 ms 
force -freeze sim:/simongame/btn_haut 1 0
run 200 ms 
force -freeze sim:/simongame/btn_haut 0 0
run 200 ms 
force -freeze sim:/simongame/btn_haut 1 0
run 200 ms 
force -freeze sim:/simongame/btn_haut 0 0
run 200 ms 
force -freeze sim:/simongame/btn_droite 1 0
run 200 ms 
force -freeze sim:/simongame/btn_droite 0 0
run 200 ms 
force -freeze sim:/simongame/btn_haut 1 0
run 200 ms 
force -freeze sim:/simongame/btn_haut 0 0
run 200 ms 
force -freeze sim:/simongame/btn_gauche 1 0
run 200 ms 
force -freeze sim:/simongame/btn_gauche 0 0
run 200 ms 
force -freeze sim:/simongame/btn_bas 1 0
run 200 ms 
force -freeze sim:/simongame/btn_bas 0 0
run 200 ms 
force -freeze sim:/simongame/btn_centre 1 0
run 200 ms 
force -freeze sim:/simongame/btn_centre 0 0
run 200 ms 
force -freeze sim:/simongame/btn_gauche 1 0
run 200 ms 
force -freeze sim:/simongame/btn_gauche 0 0
run 30100 ms

####### SEQUENCE 15 ########

force -freeze sim:/simongame/btn_droite 1 0
run 200 ms 
force -freeze sim:/simongame/btn_droite 0 0
run 200 ms 
force -freeze sim:/simongame/btn_gauche 1 0
run 200 ms 
force -freeze sim:/simongame/btn_gauche 0 0
run 200 ms 
force -freeze sim:/simongame/btn_droite 1 0
run 200 ms 
force -freeze sim:/simongame/btn_droite 0 0
run 200 ms 
force -freeze sim:/simongame/btn_bas 1 0
run 200 ms 
force -freeze sim:/simongame/btn_bas 0 0
run 200 ms 
force -freeze sim:/simongame/btn_centre 1 0
run 200 ms 
force -freeze sim:/simongame/btn_centre 0 0
run 200 ms 
force -freeze sim:/simongame/btn_droite 1 0
run 200 ms 
force -freeze sim:/simongame/btn_droite 0 0
run 200 ms 
force -freeze sim:/simongame/btn_haut 1 0
run 200 ms 
force -freeze sim:/simongame/btn_haut 0 0
run 200 ms 
force -freeze sim:/simongame/btn_haut 1 0
run 200 ms 
force -freeze sim:/simongame/btn_haut 0 0
run 200 ms 
force -freeze sim:/simongame/btn_droite 1 0
run 200 ms 
force -freeze sim:/simongame/btn_droite 0 0
run 200 ms 
force -freeze sim:/simongame/btn_haut 1 0
run 200 ms 
force -freeze sim:/simongame/btn_haut 0 0
run 200 ms 
force -freeze sim:/simongame/btn_gauche 1 0
run 200 ms 
force -freeze sim:/simongame/btn_gauche 0 0
run 200 ms 
force -freeze sim:/simongame/btn_bas 1 0
run 200 ms 
force -freeze sim:/simongame/btn_bas 0 0
run 200 ms 
force -freeze sim:/simongame/btn_centre 1 0
run 200 ms 
force -freeze sim:/simongame/btn_centre 0 0
run 200 ms 
force -freeze sim:/simongame/btn_gauche 1 0
run 200 ms 
force -freeze sim:/simongame/btn_gauche 0 0
run 200 ms 
force -freeze sim:/simongame/btn_bas 1 0
run 200 ms 
force -freeze sim:/simongame/btn_bas 0 0
run 32100 ms

####### SEQUENCE 16 ########

force -freeze sim:/simongame/btn_droite 1 0
run 200 ms 
force -freeze sim:/simongame/btn_droite 0 0
run 200 ms 
force -freeze sim:/simongame/btn_gauche 1 0
run 200 ms 
force -freeze sim:/simongame/btn_gauche 0 0
run 200 ms 
force -freeze sim:/simongame/btn_droite 1 0
run 200 ms 
force -freeze sim:/simongame/btn_droite 0 0
run 200 ms 
force -freeze sim:/simongame/btn_bas 1 0
run 200 ms 
force -freeze sim:/simongame/btn_bas 0 0
run 200 ms 
force -freeze sim:/simongame/btn_centre 1 0
run 200 ms 
force -freeze sim:/simongame/btn_centre 0 0
run 200 ms 
force -freeze sim:/simongame/btn_droite 1 0
run 200 ms 
force -freeze sim:/simongame/btn_droite 0 0
run 200 ms 
force -freeze sim:/simongame/btn_haut 1 0
run 200 ms 
force -freeze sim:/simongame/btn_haut 0 0
run 200 ms 
force -freeze sim:/simongame/btn_haut 1 0
run 200 ms 
force -freeze sim:/simongame/btn_haut 0 0
run 200 ms 
force -freeze sim:/simongame/btn_droite 1 0
run 200 ms 
force -freeze sim:/simongame/btn_droite 0 0
run 200 ms 
force -freeze sim:/simongame/btn_haut 1 0
run 200 ms 
force -freeze sim:/simongame/btn_haut 0 0
run 200 ms 
force -freeze sim:/simongame/btn_gauche 1 0
run 200 ms 
force -freeze sim:/simongame/btn_gauche 0 0
run 200 ms 
force -freeze sim:/simongame/btn_bas 1 0
run 200 ms 
force -freeze sim:/simongame/btn_bas 0 0
run 200 ms 
force -freeze sim:/simongame/btn_centre 1 0
run 200 ms 
force -freeze sim:/simongame/btn_centre 0 0
run 200 ms 
force -freeze sim:/simongame/btn_gauche 1 0
run 200 ms 
force -freeze sim:/simongame/btn_gauche 0 0
run 200 ms 
force -freeze sim:/simongame/btn_bas 1 0
run 200 ms 
force -freeze sim:/simongame/btn_bas 0 0
run 200 ms 
force -freeze sim:/simongame/btn_haut 1 0
run 200 ms 
force -freeze sim:/simongame/btn_haut 0 0
run 30000 ms


#La s�quence est : 001 010 011 101 010 011 001 100 001 001 100 101 010 100 011 100

####### TEST AVEC SEQUENCE FAUSSE #######
force -freeze sim:/simongame/clk 1 0, 0 {7812500 ps} -r {15625 ns}
force -freeze sim:/simongame/rst 1 0
force -freeze sim:/simongame/btn_gauche 0 0
force -freeze sim:/simongame/btn_droite 0 0
force -freeze sim:/simongame/btn_centre 0 0
force -freeze sim:/simongame/btn_haut 0 0
force -freeze sim:/simongame/btn_bas 0 0
run 10 ms

force -freeze sim:/simongame/rst 0 0
force -freeze sim:/simongame/btn_gauche 1 0
force -freeze sim:/simongame/btn_droite 1 0
run 1100 ms

force -freeze sim:/simongame/btn_gauche 0 0
force -freeze sim:/simongame/btn_droite 0 0
run 2000 ms

####### SEQUENCE 1 ########

force -freeze sim:/simongame/btn_droite 1 0
run 200 ms 
force -freeze sim:/simongame/btn_droite 0 0
run 4100 ms

####### SEQUENCE 2 ########

force -freeze sim:/simongame/btn_droite 1 0
run 200 ms 
force -freeze sim:/simongame/btn_droite 0 0
run 200 ms 
force -freeze sim:/simongame/btn_gauche 1 0
run 200 ms 
force -freeze sim:/simongame/btn_gauche 0 0
run 6100 ms

####### SEQUENCE 3 ########

force -freeze sim:/simongame/btn_droite 1 0
run 200 ms 
force -freeze sim:/simongame/btn_droite 0 0
run 200 ms 
force -freeze sim:/simongame/btn_gauche 1 0
run 200 ms 
force -freeze sim:/simongame/btn_gauche 0 0
run 200 ms 
force -freeze sim:/simongame/btn_gauche 1 0
run 200 ms 
force -freeze sim:/simongame/btn_gauche 0 0
run 30000 ms
