----------------------------------------------------------------------------------
-- Company: HEIA-FR
-- Engineer: Naël Telfser
-- 
-- Create Date:    11:54:35 05/19/2021 
-- Design Name: 	Simon_Game
-- Module Name:    Son_Clock_209Hz - Behavioral 
-- Project Name: 	TN2 - Projet Inegré - Groupe 5
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Son_Clock_209Hz is
    Port ( clk : in  STD_LOGIC;
           rst : in  STD_LOGIC;
           etat_son : in  STD_LOGIC_VECTOR (3 downto 0);
           signal_209hz : out  STD_LOGIC);
end Son_Clock_209Hz;

architecture Behavioral of Son_Clock_209Hz is
	type etats is (bas, haut);

	signal etat_present_209hz, etat_futur_209hz : etats;
	signal compteur_present_209hz, compteur_futur_209hz : Unsigned(7 downto 0) := (others => '0');
	constant half_period_209hz : Unsigned(7 downto 0) := to_unsigned(153, 8);
	signal compte_209hz, fin_comptage_209hz : std_logic;
begin
	compte_209hz <= '1' when etat_son = "0010" else '0';
	fin_comptage_209hz <= '1' when compteur_present_209hz = half_period_209hz else '0';

	combi_209hz : process(etat_present_209hz, compte_209hz, fin_comptage_209hz)
	begin
		case( etat_present_209hz ) is
		
			when bas =>
				if fin_comptage_209hz = '1' then
					etat_futur_209hz <= haut;
				else
					etat_futur_209hz <= bas;
				end if ;
			when haut =>
				if compte_209hz = '0' or fin_comptage_209hz = '1' then
					etat_futur_209hz <= bas;
				else
					etat_futur_209hz <= haut;
				end if ;
			when others => etat_futur_209hz <= bas;
		end case ;
	end process;

	registre_209hz: process(clk, rst)
	begin
		if rst = '1' then
			etat_present_209hz <= bas;
		elsif rising_edge(clk) then
			etat_present_209hz <= etat_futur_209hz;
		end if ;
	end process;

	registre_compteur_209hz: process(clk, rst)
	begin
		if rst = '1' then
			compteur_present_209hz <= (others => '0');
		elsif rising_edge(clk) then
			compteur_present_209hz <= compteur_futur_209hz;
		end if ;
	end process;

	combi_compteur_209hz: process(compteur_present_209hz, compte_209hz, fin_comptage_209hz)
	begin
		if compte_209hz = '0' or fin_comptage_209hz = '1' then
			compteur_futur_209hz <= (others => '0');
		else
			compteur_futur_209hz <= compteur_present_209hz + 1;
		end if ;
	end process;
	signal_209hz <= '1' when etat_present_209hz = haut else '0';
end Behavioral;

