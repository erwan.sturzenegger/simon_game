force -freeze sim:/son_clock_3_3hz/clk 1 0, 0 {7812500 ps} -r {15625 ns}
# Test état 3_3hz
force -freeze sim:/son_clock_3_3hz/rst 0 0
force -freeze sim:/son_clock_3_3hz/etat_son 0001 0
run 1000 ms
# Test reset
force -freeze sim:/son_clock_3_3hz/rst 1 0
force -freeze sim:/son_clock_3_3hz/etat_son 0000 0
run 1000 ms
# Test état 3_3hz
force -freeze sim:/son_clock_3_3hz/rst 0 0
force -freeze sim:/son_clock_3_3hz/etat_son 0001 0
run 1000 ms
# Test autre état
force -freeze sim:/son_clock_3_3hz/rst 0 0
force -freeze sim:/son_clock_3_3hz/etat_son 1111 0
run 1000 ms