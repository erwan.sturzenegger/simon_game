do {GenerateurSymboles.fdo} 

force -freeze sim:/generateursymboles/clk 1 0, 0 {7812500 ps} -r {15625 ns}
force -freeze sim:/generateursymboles/etat_jeu 000 0
force -freeze sim:/generateursymboles/sequence 001101100011010001101100011010001101100011010001 0
force -freeze sim:/generateursymboles/no_sequence 00001 0
force -freeze sim:/generateursymboles/rst 1 0
run 5 ms
force -freeze sim:/generateursymboles/rst 0 0

run 500 ms

force -freeze sim:/generateursymboles/rst 0 0
force -freeze sim:/generateursymboles/etat_jeu 001 0
force -freeze sim:/generateursymboles/sequence 001101100011010001101100011010001101100011010001 0
force -freeze sim:/generateursymboles/no_sequence 01000 0

run 15300 ms

force -freeze sim:/generateursymboles/etat_jeu 011 0

run 1500 ms

force -freeze sim:/generateursymboles/etat_jeu 100 0

run 1500 ms