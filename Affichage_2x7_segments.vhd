library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.all;
use IEEE.NUMERIC_STD.all;

entity Affichage_2x7_segments is

    Port ( no_sequence : IN STD_LOGIC_VECTOR(4 downto 0);
           etat_jeu : IN STD_LOGIC_VECTOR(2 downto 0);
           etat_systeme : IN STD_LOGIC;

           unite : OUT STD_LOGIC_VECTOR(6 downto 0);
           dizaine : OUT STD_LOGIC_VECTOR(6 downto 0));

end Affichage_2x7_segments;

architecture Behavioral of Affichage_2x7_segments is

begin

setDisplay:PROCESS(no_sequence, etat_jeu, etat_systeme)
  begin

    IF etat_jeu = "010" AND etat_jeu = "010" THEN
      case no_sequence IS
        when "00000" => dizaine <= "0000000"; unite <= "1111110"; -- 0

        when "00001" => dizaine <= "0000000"; unite <= "0110000"; -- 1

        when "00010" => dizaine <= "0000000"; unite <= "1101101"; -- "2"

        when "00011" => dizaine <= "0000000"; unite <= "1111001"; -- "3"

        when "00100" => dizaine <= "0000000"; unite <= "0110011"; -- "4"

        when "00101" => dizaine <= "0000000"; unite <= "1011011"; -- "5"

        when "00110" => dizaine <= "0000000"; unite <= "1011111"; -- "6"

        when "00111" => dizaine <= "0000000"; unite <= "1110000"; -- "7"

        when "01000" => dizaine <= "0000000"; unite <= "1111111"; -- "8"

        when "01001" => dizaine <= "0000000"; unite <= "1111011"; -- "9"

        when "01010" => dizaine <= "0110000"; unite <= "1111110";

        when "01011" => dizaine <= "0110000"; unite <= "0110000";

        when "01100" => dizaine <= "0110000"; unite <= "1101101";

        when "01101" => dizaine <= "0110000"; unite <= "1111001";

        when "01110" => dizaine <= "0110000"; unite <= "0110011";

        when "01111" => dizaine <= "0110000"; unite <= "1011011";

        when "10000" => dizaine <= "0110000"; unite <= "1011111";

        when others =>
        unite <= "0000000";
        dizaine <= "0000000";
        end case;
      ELSE
        unite <= "0000000";
        dizaine <= "0000000";
      END IF;
  end PROCESS setDisplay;
end Behavioral;
