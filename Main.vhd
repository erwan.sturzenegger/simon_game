library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity Main is
    Port ( clk : in  STD_LOGIC;
           rst : in  STD_LOGIC;
           etat_jeu : in  STD_LOGIC_VECTOR (2 downto 0);
           start : in  STD_LOGIC;

           etat_systeme : out  STD_LOGIC);
end Main;

architecture Behavioral of Main is

  TYPE etats IS (GO, PLAY); -- déclaration des états
  SIGNAL etat_present, etat_futur : etats;

  BEGIN
    comb_future: PROCESS(etat_present, start, etat_jeu)
    BEGIN
      CASE etat_present IS
        WHEN GO =>
          IF start = '1' THEN
            etat_futur <= PLAY;
          ELSE
            etat_futur <= GO;
          END IF;
        WHEN PLAY =>
          IF start = '0' AND etat_jeu = "000" THEN
            etat_futur <= GO;
          ELSE
            etat_futur <= PLAY;
          END IF;
          WHEN others => etat_futur <= GO; -- ETAT PARAS.
        END CASE;
      END PROCESS comb_future;

      registre: PROCESS(clk, rst) IS
      BEGIN
        IF rst= '1' THEN -- execute reset, jeu s'arrete
          etat_present <= GO;
        ELSIF rising_edge(clk) THEN -- rising_edge IS clk'event
          etat_present <= etat_futur;
        END IF;
      END PROCESS registre;

      etat_systeme <= '1' when etat_present = PLAY else '0';
end Behavioral;
